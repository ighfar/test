import React,{Component} from 'react';
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { Link } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import user from "../../../images/user.png";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import {toast, ToastContainer} from "react-toastify";
import {history} from "../../../shared/configure-store";

require('dotenv').config();
const validator = new SimpleReactValidator({locale: process.env.REACT_APP_LOCALE});

class UsersEdit extends Component {
    constructor(props) {

        super(props);

        this.state = {
            loading: true,
            loadingButton: false,
            errors: {},

            roles: [],

            id: '',
            username: '',
            name: '',
            email: '',
            password: '',
            phone: '',
            address: '',
            role: '',
            photo: user,
            avatar: '',
            roleId: '',
            bod: '',

            rows: [],
            sendPageNumber: '',
            sendPerPage: '',
            total: 0,
            perPage: 10,
            currentPage: 1,
            currentPageTable: 0,
            totalData: 0,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {

        document.title = 'Admin - Edit User';

        const id = this.props.match.params.id;

        let currentDate = new Date();

        Api.get('/roles').then(resp => {

            if (resp.data) {

                this.setState({
                    roles: resp.data,
                })

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/users/'+id).then(resp => {

            if (resp.data) {
                let data = resp.data;
                this.setState({
                    id: data.id,
                    username: data.username,
                    name: data.name,
                    email: data.email,
                    password: '',
                    phone: data.phone,
                    photo: data.avatar !== null ? process.env.REACT_APP_API_STORAGE_PATH+data.avatar : user,
                    address: data.address,
                    bod: data.birth_date !== null ? data.birth_date : currentDate.getFullYear() + '-' + appendLeadingZeroes(currentDate.getMonth() + 1) + '-' + appendLeadingZeroes(currentDate.getDate()),
                    roleId: data.roles ? data.roles.id : null,
            });

            }

        }).catch(err => {
            console.log(err);
        });



    }

    handleChange (e, prop){

        this.setState({

            [prop]: e.target.value

        })

    };

    handleDate (date, prop){

        this.setState({

            [prop]: date

        })

    };

    showMessage = (status, message) => {
        if(status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    handleSubmit = (e) => {

        e.preventDefault();

        if (!validator.allValid()) {

            this.setState({
                    errors: validator.getErrorMessages()
                }
            );

            return false;

        }

        this.setState({
                errors: {},
                loadingButton: true,
            }
        );

        let formData = new FormData();
        formData.append('username', this.state.username);
        formData.append('name', this.state.name);
        formData.append('email', this.state.email);
        formData.append('phone', this.state.phone);
        if(this.state.password) {
            formData.append('password', this.state.password);
        }
        formData.append('address', this.state.address);
        formData.append('birth', this.state.bod);
        formData.append('roleId', this.state.roleId);
        if(this.state.avatar) {
            formData.append('avatar', this.state.avatar);
        }


        Api.putFile('/users/'+this.state.id,{
            method: 'POST',
            body: formData
        }).then(resp => {

            this.setState({
                loadingButton: false,
                }
            );

            history.push('/users');

            this.showMessage(true, 'User successfully added');

        }).catch(err => {

            if(ErrorHandling.checkErrorTokenExpired(err)){

            }else{

                this.setState({
                        errors: err.errors,
                        loadingButton: false
                    }
                );

                this.showMessage(false, 'Invalid format data');
            }
        });

    };

    handleClickShowPassword = () => {

        this.setState({

            showPassword:(!this.state.showPassword)

        })

    };

    handleGo  = (link) => {

        history.push(link);
    };

    handleAvatar = e => {
        this.setState({
            photo: URL.createObjectURL(e.target.files[0]),
            avatar: e.target.files[0]
        });
    };

    render() {
        return (
            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h1 className="page-title">Edit Users</h1>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                            <li className="breadcrumb-item"><Link to="/users" >Users</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Edit</li>
                        </ol>
                    </nav>

                </div>
                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                        <form name="add" id="addUser" className="row" noValidate>
                            <div className="col-12">
                                <div className="form-group">
                                    <img src={this.state.photo} alt="user" className="user-photo mr-2" />
                                    <TextField variant="outlined"
                                               type="file"
                                               id='avatar'
                                               name="avatar"
                                               className="avatar d-inline"
                                               onChange={this.handleAvatar}
                                    />
                                    <label htmlFor="avatar" className="label-file btn btn-blue">Upload Avatar</label>
                                    <div className='text-danger'>{this.state.errors.avatar}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Username</label>
                                    <TextField variant="outlined"
                                               type='text'
                                               id='username'
                                               name="username"
                                               label="Username"
                                               onChange={(e) => this.handleChange(e, 'username')}
                                               minLength={4}
                                               maxLength={50}
                                               value={this.state.username}
                                               fullWidth
                                    />
                                    {validator.message('username', this.state.username, 'required')}
                                    <div className='text-danger'>{this.state.errors.username}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Full Name</label>
                                    <TextField variant="outlined"
                                               type='text'
                                               id='name'
                                               name="name"
                                               label="Full Name"
                                               onChange={(e) => this.handleChange(e, 'name')}
                                               minLength={4}
                                               maxLength={50}
                                               value={this.state.name}
                                               fullWidth
                                    />
                                    {validator.message('name', this.state.name, 'required')}
                                    <div className='text-danger'>{this.state.errors.name}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Email</label>
                                    <TextField variant="outlined"
                                               type='text'
                                               id='email'
                                               name="email"
                                               label="Email"
                                               onChange={(e) => this.handleChange(e, 'email')}
                                               minLength={4}
                                               maxLength={50}
                                               value={this.state.email}
                                               fullWidth
                                    />
                                    {validator.message('email', this.state.email, 'email|required')}
                                    <div className='text-danger'>{this.state.errors.email}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Password</label>
                                    <TextField variant="outlined"
                                               type={this.state.showPassword ? 'text' : 'password'}
                                               id="adornment-password"
                                               name="password"
                                               label="Password"
                                               onChange={(e) => this.handleChange(e,'password')}
                                               value={this.state.password}
                                               fullWidth
                                               InputProps={{
                                                   endAdornment: (
                                                       <InputAdornment position="end">
                                                           <IconButton
                                                               aria-label="Toggle password visibility"
                                                               onClick={this.handleClickShowPassword}
                                                           >
                                                               {this.state.showPassword ? <i className="fas fa-eye"> </i> :
                                                                   <i className="fas fa-eye-slash"> </i>}
                                                           </IconButton>
                                                       </InputAdornment>
                                                   )
                                               }}
                                    />
                                    {validator.message('password', this.state.password, 'min:8')}
                                    <div className='text-danger'>{this.state.errors.password}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Date of Birth</label>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <DatePicker
                                            label="Birth Date"
                                            value={this.state.bod}
                                            onChange={(date) => this.handleDate(date, 'bod')}
                                            format={'yyyy-MM-dd'}
                                            cancelLabel="BATAL"
                                            inputVariant="outlined"
                                        />
                                    </MuiPickersUtilsProvider>
                                    <div className='text-danger'>{this.state.errors.bod}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Address</label>
                                    <TextField variant="outlined"
                                               type='text'
                                               id='address'
                                               name="address"
                                               label="Address"
                                               onChange={(e) => this.handleChange(e, 'address')}
                                               minLength={4}
                                               maxLength={50}
                                               value={this.state.address}
                                               fullWidth
                                    />
                                    <div className='text-danger'>{this.state.errors.address}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Phone</label>
                                    <TextField variant="outlined"
                                               type='number'
                                               id='phone'
                                               name="phone"
                                               label="Phone"
                                               onChange={(e) => this.handleChange(e, 'phone')}
                                               minLength={4}
                                               maxLength={50}
                                               value={this.state.phone}
                                               fullWidth
                                    />
                                    {validator.message('phone', this.state.phone, 'required')}
                                    <div className='text-danger'>{this.state.errors.phone}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Role</label>
                                    <TextField variant="outlined"
                                               select
                                               id="roleId"
                                               name="roleId"
                                               label="Role"
                                               onChange={(e) => this.handleChange(e, 'roleId')}
                                               minLength={4}
                                               maxLength={50}
                                               value={this.state.roleId}
                                               fullWidth
                                    >
                                        {this.state.roles.map(option => (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.display_name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    {validator.message('roleId', this.state.roleId, 'required')}
                                    <div className='text-danger'>{this.state.errors.roleId}</div>
                                </div>
                            </div>

                            <div className="col-12 text-center">
                                <Button
                                    variant="contained"
                                    className="mr-3"
                                    onClick={() => this.handleGo('/users')}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className=""
                                    onClick={this.handleSubmit}
                                    disabled={this.state.loadingButton && 'disabled' }
                                >
                                    Submit{ this.state.loadingButton && <i className="fa fa-spinner fa-spin"> </i> }
                                </Button>
                            </div>
                        </form>
                    </div>
                </div>

                <ToastContainer />

            </div>
        )
    }
}

export default UsersEdit;

function appendLeadingZeroes(n) {
    if (n <= 9) {
        return "0" + n;
    }
    return n
}