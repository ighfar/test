import React from 'react';
import TextField from "@material-ui/core/TextField";
import { Link } from 'react-router-dom';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";

require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

export default function BannerEdit(props) {

    const [loading, setLoading] = React.useState(false);
    const [errors, setErrors] = React.useState([]);
    const [id, setId] = React.useState('');
    const [nomer, setNomer] = React.useState('');
    const [image, setImage] = React.useState('');
    const [simbol, setSimbol] = React.useState({
        file: [],
        filepreview: null,
    });

    const [invalidImage, setinvalidImage] = React.useState(null);
    let reader = new FileReader();

    React.useEffect(() => {

        document.title = 'Halaman Rubah Banner';

        const idBanner = props.match.params.id;

        Api.get('/banners/' + idBanner).then(resp => {

            if (resp.data) {
                let data = resp.data;
                setId(data.id);
                setNomer(data.ordering);
                setImage(data.image);
            }

        }).catch(err => {
            console.log(err);
        });

    }, []);


    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    const handleSubmit = async () => {

        if (!validator.allValid()) {

            setErrors(validator.getErrorMessages());

            return false;

        }

        setLoading(true);

        const formdata = new FormData()
        formdata.append('ordering', nomer);
        formdata.append('image', simbol.file);

        Api.putFile(`/banners/${id}`, {
            method: 'POST',
            body: formdata
        }).then(resp => {

            setLoading(false);

            history.push('/banner');

            showMessage(true, 'Berhasil merubah banner.');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                setErrors(err.errors);
                setLoading(false);

                showMessage(false, 'Invalid format data');
            }
        });
    }


    const handleBack = () => {
        history.push('/banner');
    };

    const onChangeImage = (event) => {
        const imageFile = event.target.files[0];
        const imageFileName = event.target.files[0].name;

        if (!imageFile) {
            setinvalidImage('Please select image.');
            return false;
        }

        if (!imageFile.name.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG|gif)$/)) {
            setinvalidImage('Please select valid image JPG,JPEG,PNG');
            return false;
        }
        reader.onload = (e) => {
            const img = new Image();
            img.onload = () => {

                //------------- Resize img code ----------------------------------
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = 437;
                var MAX_HEIGHT = 437;
                var width = img.width;
                var height = img.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
  
                ctx.drawImage(img, 0, 0, width, height);
                ctx.canvas.toBlob((blob) => {
                    const file = new File([blob], imageFileName, {
                        type: 'image/jpeg',
                        lastModified: Date.now()
                    });
                    setSimbol({
                        ...simbol,
                        file: file,
                        filepreview: URL.createObjectURL(imageFile),
                    })
                }, 'image/jpeg', 1);
                setinvalidImage(null)
            };
            img.onerror = () => {
                setinvalidImage('Invalid image content.');
                return false;
            };
            //debugger
            img.src = e.target.result;
        };
        reader.readAsDataURL(imageFile);
    }


    return (
        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h1 className="page-title">Rubah Banner</h1>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                        <li className="breadcrumb-item"><Link to="/regulations" >Banner</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Rubah</li>
                    </ol>
                </nav>

            </div>
            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">
                    <form name="add" id="addUser" className="row" noValidate>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Nomer Urut <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='nomer_urut'
                                    name="nomer_urut"
                                    label="Nomer Urut"
                                    onChange={e => setNomer(e.target.value)}
                                    value={nomer}
                                    fullWidth
                                />
                                {validator.message('nomer_urut', nomer, 'required')}
                                <div className='text-danger'>{errors.nomer_urut}</div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            {invalidImage !== null ? <h4 className="error"> {invalidImage} </h4> : null}
                            <div className="form-group">
                                <label>Gambar Banner</label>
                                <input type="file" className="form-control" name="image" onChange={onChangeImage} />
                            </div>

                            <div className="form-group">
                                <label>Preview</label>
                                <div className="text-center">
                                    {simbol.filepreview !== null ?
                                        <img className="previewimg" src={simbol.filepreview} alt="UploadImage" />
                                        : <img src={process.env.REACT_APP_API_STORAGE_PATH + image} alt='preview simbol' className="" />}
                                </div>
                            </div>
                        </div>

                        <div className="col-12 text-left">
                            <Button
                                variant="contained"
                                className="mr-3"
                                onClick={handleBack}
                            >
                                Kembali
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className=""
                                onClick={handleSubmit}
                                disabled={loading && 'disabled'}
                            >
                                Simpan{loading && <i className="fa fa-spinner fa-spin"> </i>}
                            </Button>
                        </div>
                    </form>
                </div>
            </div>

            <ToastContainer />

        </div>
    )

}