import React from "react";
import Api from "../../../libraries/api";
import { Chart } from "react-google-charts";
import { useDispatch } from "react-redux";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { CircularProgress } from "@material-ui/core";

function ChartAdminKandidat() {
  const [loading, setLoading] = React.useState(false);
  const [chart, setChart] = React.useState([]);
  const [colors, setColors] = React.useState([]);
  const [lists, setLists] = React.useState([]);

  const dispatch = useDispatch();

  React.useEffect(() => {
    setTimeout(() => {
      fetchChart();
    }, 3000);
  }, [dispatch]);

  const fetchChart = async () => {
    setLoading(true);

    Api.get(`/dashboards/pie-chart`)
      .then((resp) => {
        if (resp) {
          const data = resp;
          console.log(resp);

          setChart(data.data);
          setColors(data.colors);
          setLists(data.list);

          setLoading(false);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  const options = {
    pieHole: 0.4,
    // is3D: false,
    colors: colors,
    legend: "none",
    chartArea: {
      left: 5,
      top: 20,
      width: "100%",
      height: "350",
    },
  };

  function valuetext(value) {
    return `${value}%`;
  }

  console.log(chart);

  return (
    <>
      <Chart
        chartType="PieChart"
        width="100%"
        height="400px"
        data={chart}
        options={options}
      />

      <Table className="table-list" size="small">
        <TableHead>
          <TableRow>
            <TableCell>Kandidat</TableCell>
            <TableCell>Partai</TableCell>
            <TableCell>Pemilihan</TableCell>
            <TableCell>Total</TableCell>
            <TableCell>Persentase</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {loading ? (
            <TableRow>
              <TableCell colSpan={16} align="center" className="py-5">
                <CircularProgress color="primary" />
              </TableCell>
            </TableRow>
          ) : lists.length === 0 ? (
            <TableRow>
              <TableCell colSpan={6} style={{ textAlign: "center" }}>
                Belum ada data.
              </TableCell>
            </TableRow>
          ) : (
            lists.map((row, key) => (
              <TableRow key={key} selected={false}>
                <TableCell>
                  <span>{row.name === null ? "" : row.name}</span>
                </TableCell>
                <TableCell>
                  <span>{row.partai === null ? "" : row.partai}</span>
                </TableCell>
                <TableCell>
                  <span>{row.campaign === null ? "" : row.campaign}</span>
                </TableCell>
                <TableCell>
                  <span>{parseInt(row.total).toLocaleString()}</span>
                </TableCell>
                <TableCell>
                  <span>{row.percentage === null ? "" : row.percentage}</span>
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>

      {/* {loading ? (
                <LinearProgress />
            ) : (
                lists.map(option => (
                    <div className="list-kandidat" key={option.name}>
                        <div className="list1">
                            <span className="partai">{option.partai}</span>
                            <span className="campaign">{option.campaign}</span>
                        </div>
                        <div className="list2">
                            {option.name}
                        </div>
                        <div className="list3">
                            <Slider
                                defaultValue={option.percentage}
                                valueLabelFormat={valuetext}
                                aria-labelledby="discrete-slider-always"
                                step={10}
                                disabled
                                valueLabelDisplay="on"
                            />
                        </div>
                        <div className="pemilih">
                            <span>{parseInt(option.total).toLocaleString()}</span>
                            <span>Pendukung</span>
                        </div>
                    </div>
                ))
            )
            } */}
    </>
  );
}

export default ChartAdminKandidat;
