import React, { useMemo } from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { Chart } from "react-google-charts";
import Box from  "@material-ui/core/Box";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

import Api from "../../../libraries/api";

export default function Home(props) {
  const [loading, setLoading] = React.useState(false);
  const [status, setStatus] = React.useState([]);
  const [date, setDate] = React.useState("0");
  const [total, setTotal] = React.useState("0");
  const [totalPolling, setTotalPolling] = React.useState("0");
  const [totalVoters, setTotalVoters] = React.useState("0");
  const [statusPasti, setStatusPasti] = React.useState("");
  const [statusMungkin, setStatusMungkin] = React.useState("");
  const [statusTidak, setStatusTidak] = React.useState("");
  const [campaign, setCampaign] = React.useState({});
  const [campaignId, setCampaignId] = React.useState({});
  const role_state = useSelector((state) => state.role_state);
  const [data, setData] = React.useState([]);
  const [chart, setChart] = React.useState([]);
  const [colors, setColors] = React.useState([]);
  const [percentage, setPercentage] = React.useState("");

  React.useEffect(() => {
    Api.get("/profile")
      .then((resp) => {
        if (resp) {
          setCampaign(resp.campaign);
          setCampaignId(resp.campaign.id);
        }
      })
      .catch((err) => {
        console.log(err);
      });

    let route = "/dashboards/peta-suara";

    let formData = new FormData();
    formData.append("skipCache", true);

    Api.putFile(route, {
      method: "POST",
      body: formData,
    })
      .then((resp) => {
        if (resp) {
          const dataSuara = resp.data;
          setData(dataSuara);
        }
      })
      .catch((err) => {
        console.log(err);
      });

    fetchDashboardAdmin();
  }, [role_state]);

  const fetchDashboardAdmin = async () => {
    setLoading(true);

    Api.get("/dashboards")
      .then((resp) => {
        if (resp) {
          const data = resp;

          setStatus(data.status);
          setTotal(data.total);
          setDate(data.date);
          setPercentage(data.percentage);
          setTotalVoters(data.total_voters);
          setTotalPolling(data.total_polling);
          setLoading(false);
          setChart(data.status_chart.data);
          setColors(data.status_chart.colors);
          setStatusPasti(data.status_pasti);
          setStatusMungkin(data.status_mungkin);
          setStatusTidak(data.status_tidak);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  const options = {
    legend: "none",
  };

  const dataChart = useMemo(() => {
    const _temp = [];
    chart.forEach((item, index) => {
      if (index === 0) {
        _temp.push([
          `${item[0]}`,
          `${item[1]}`,
          { role: "style" },
          {
            sourceColumn: 0,
            role: "annotation",
            type: "string",
            calc: "stringify",
          },
        ]);
      } else {
        _temp.push([`${item[0]}`, Number(item[1]), colors[index - 1], null]);
      }
    });
    return _temp;
  }, [chart, colors]);

  return (
    <div className="row main-content">
      <div className="col-12 px-lg-5">
        {role_state != "superadmin"  && (
          <>
            <div className="row">
              <div className="col-12 col-md-12 col-xl-12">
                <div className="card-info white">
                  <div className="card-info white">
                    <div className="d-flex">
                      <img
                        src={
                          campaign ? process.env.REACT_APP_API_STORAGE_PATH + campaign.image : ''
                        }
                        alt="user"
                        className="user-photo mr-2"
                      />
                      <div className="d-flex flex-column">
                        <label className="label-grey">
                          <b>#ID - {campaign ? campaign.id_akun : ''}</b>
                        </label>
                        <small>
                          <b>{`${campaign ? campaign.pemilihan : ''} (${campaign ? campaign.dapil_name : ''})`}</b>
                        </small>
                        <h2>{campaign ? campaign.name : ''}</h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-3">
                <div className="card-info white">
                  <div className="row align-items-center">
                    <div className="col-8 col-md-10 col-xl-8">
                      <label className="label-grey">Pemilu</label>
                      <p className="number">{date}</p>
                      <p>Hari Lagi</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card-info white">
                  <div className="row align-items-center">
                    <div className="col-8 col-md-10 col-xl-8">
                      <label className="label-grey">Total Data Pemilih</label>
                      <p className="number">
                        {parseInt(totalVoters).toLocaleString()}
                      </p>
                      <p>Pemilih</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <Link
                  to={`/capel?campaign=${campaignId}&filter=pasti`}
                  target="_blank"
                >
                  <div className="card-info white">
                    <div className="row align-items-center">
                      <div className="col-8 col-md-10 col-xl-8">
                        <label className="label-grey">Pasti Dipilih</label>
                        <p className="number">{`${parseInt(
                          statusPasti
                        ).toLocaleString()}`}</p>
                        <p>Dukungan</p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link
                  to={`/capel?campaign=${campaignId}&filter=mungkin`}
                  target="_blank"
                >
                  <div className="card-info white">
                    <div className="row align-items-center">
                      <div className="col-8 col-md-10 col-xl-8">
                        <label className="label-grey">Mungkin Dipilih</label>
                        <p className="number">{`${parseInt(
                          statusMungkin
                        ).toLocaleString()}`}</p>
                        <p>Dukungan</p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            </div>

            <div className="row">
              <div className="col-md-3">
                <Link
                  to={`/capel?campaign=${campaignId}&filter=tidak`}
                  target="_blank"
                >
                  <div className="card-info white">
                    <div className="row align-items-center">
                      <div className="col-8 col-md-10 col-xl-8">
                        <label className="label-grey">Tidak Yakin</label>
                        <p className="number">{`${parseInt(
                          statusTidak
                        ).toLocaleString()}`}</p>
                        <p>Dukungan</p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link to={`/capel?campaign=${campaignId}`} target="_blank">
                  <div className="card-info white">
                    <div className="row align-items-center">
                      <div className="col-8 col-md-10 col-xl-8">
                        <label className="label-grey">Total Dukungan</label>
                        <p className="number">
                          {parseInt(total).toLocaleString()}
                        </p>
                        <p>Dukungan</p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link to="/capel" target="_blank">
                  <div className="card-info white">
                    <div className="row align-items-center">
                      <div className="col-8 col-md-10 col-xl-8">
                        <label className="label-grey">Total Polling</label>
                        <p className="number">
                          {parseInt(totalPolling).toLocaleString()}
                        </p>
                        <p>Poling</p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link to="/capel" target="_blank">
                  <div className="card-info white">
                    <div className="row align-items-center">
                      <div className="col-8 col-md-10 col-xl-8">
                        <label className="label-grey">Persentase</label>
                        <p className="number">
                          {((totalPolling / totalVoters) * 100).toFixed(2) +
                            "%"}
                        </p>
                        <p>Dari total data pemilih</p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            </div>

            <div className="row">
              <div className="col-xl-8 col-12">
                <Link to="/capel" target="_blank">
                  <div className="card-info white">
                    <div className="row align-items-start">
                      <div className="col-12" style={{ overflow: "hidden" }}>
                        <Chart
                          chartType="BarChart"
                          //   width={650}
                          //   height={600}
                          data={dataChart}
                          options={options}
                          style={{
                            minHeight: 900,
                            minWidth: 600,
                            overflow: "hidden",
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </Link>
              </div>

              <div className="col-xl-4 col-12">
                <Link to="/capel" target="_blank">
                  <div className="card-info white" style={{ height: 650 }}>
                    <div className="row align-items-center">
                      <div className="col-8 col-md-10 col-xl-8 h-100">
                        <Table className="table-list" size="small">
                          <TableBody>
                            {loading ? (
                              <TableRow>
                                <TableCell
                                  colSpan={16}
                                  align="center"
                                  className="py-5"
                                >
                                  <LinearProgress color="primary" />
                                </TableCell>
                              </TableRow>
                            ) : data.length === 0 ? (
                              <TableRow>
                                <TableCell
                                  colSpan={6}
                                  style={{ textAlign: "center" }}
                                >
                                  Belum ada data.
                                </TableCell>
                              </TableRow>
                            ) : (
                              data.map((row, key) => (
                                <TableRow key={key} selected={false} >
                      
                     
                                  <TableCell>
                                    <span>
                                      {row.name === null ? "" : row.name}
                                    </span>
                                  </TableCell>
                                
                                  <TableCell>
                                    <span>
                                      {row.jumlah_suara === null
                                        ? ""
                                        : row.jumlah_suara}
                                    </span>
                             </TableCell>
                                </TableRow>
                              ))
                            )}
                          </TableBody>
                        </Table>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
}

