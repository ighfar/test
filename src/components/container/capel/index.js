import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import { ToastContainer } from "react-toastify";
import TablePagination from "@material-ui/core/TablePagination";
import TablePaginationActions from "../../presentational/table-pagination-actions";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import TuneIcon from '@material-ui/icons/Tune';
import { useDispatch, useSelector } from "react-redux";
import { payloadProvice } from "../../../redux/actions/area";
import { payloadKabupaten } from "../../../redux/actions/kabupaten";
import { payloadKecamatan } from "../../../redux/actions/kecamatan";
import { payloadKelurahan } from "../../../redux/actions/kelurahan";
import { payloadTimses } from "../../../redux/actions/timses";

require('dotenv').config();

export default function Capel(props) {

    const { loadingProvince, errorProvince, dataProvince } = useSelector((state) => state.province_state);
    const { loadingKabupaten, errorKabupaten, dataKabupaten } = useSelector((state) => state.kabupaten_state);
    const { loadingKecamatan, errorKecamatan, dataKecamatan } = useSelector((state) => state.kecamatan_state);
    const { loadingKelurahan, errorKelurahan, dataKelurahan } = useSelector((state) => state.kelurahan_state);
    const { loadingTimses, errorTimses, dataTimses } = useSelector((state) => state.timses_state);

    const [dataCampaign, setDataCampaign] = React.useState([]);

    const [campaignId, setCampaignId] = React.useState('');
    const [kodeProvince, setKodeProvince] = React.useState('11');
    const [kodeKabupaten, setKodeKabupaten] = React.useState('');
    const [kodeKecamatan, setKodeKecamatan] = React.useState('');
    const [kodeDesa, setKodeDesa] = React.useState('');
    const [kodeTimses, setTimses] = React.useState('');

    const [loading, setLoading] = React.useState(true);
    const [loadingButton, setLoadingButton] = React.useState(false);
    const [orderBy, setOrderBy] = React.useState('');
    const [sortedBy, setSortedBy] = React.useState('');
    const [searchBy, setSearchBy] = React.useState('');
    const [rows, setRows] = React.useState([]);
    const [dataFilter, setDataFilter] = React.useState([]);
    const [filterStatus, setFilterStatus] = React.useState('');
    const [total, setTotal] = React.useState(0);
    const [perPage, setPerPage] = React.useState(50);
    const [currentPage, setCurrentPage] = React.useState(1);
    const [currentPageTable, setCurrentPageTable] = React.useState(0);
    const [showDialogFilter, setShowDialogFilter] = React.useState(false);

    const dispatch = useDispatch();

    React.useEffect(() => {

        document.title = 'Admin suarapemilu - Halaman Data Pendukung';

        const params = new URLSearchParams(window.location.search);
        let filter = params.get('filter');

        let campaign = params.get('campaign');

        if (params.has('campaign')) {
            setCampaignId(campaign);
        }

        let filterStatus = '';
        if (params.has('filter')) {
           
            switch (filter) {
                case 'pasti':
                    filterStatus = 'Pasti dipilih';
                    break;
                case 'tidak':
                    filterStatus = 'Tidak yakin';
                    break;
                case 'mungkin':
                    filterStatus = 'Mungkin dipilih';
                    break;
                default:
                    filterStatus = 'Mungkin dipilih';
            }
            if (dataFilter.length > 1) {
                dataFilter.push(filterStatus);
            } else {
                setDataFilter([filterStatus]);
            }

            setFilterStatus(filterStatus);
        } 

        fetchData(false, filterStatus);

        dispatch(payloadProvice());
        dispatch(payloadKabupaten());
        dispatch(payloadKecamatan(''));
        dispatch(payloadKelurahan(''));
        dispatch(payloadTimses());

        if (dataKabupaten.length === 1) {
            setKodeKabupaten(dataKabupaten[0].kode_kabupaten);
        }

        if (dataKecamatan.length === 1) {
            setKodeKecamatan(dataKecamatan[0].kode_kecamatan);
        }

        Api.get('/select/campaigns').then(resp => {

            if (resp.data) {
                setDataCampaign(resp.data);
            }

        }).catch(err => {
            console.log(err);
        });

    }, [loading]);


    const fetchData = async (update, filterStatus) => {

        console.log(filterStatus)

        let page = update ? parseInt(currentPage + 1) : currentPage;

        let route = '/voters/campaign';

        let formData = new FormData();
        formData.append('skipCache', true);
        formData.append('limit', perPage);
        formData.append('orderBy', orderBy);
        formData.append('sortedBy', sortedBy);

        if (!searchBy) {
            formData.append('page', page);
        } else {
            formData.append('search', searchBy);
        }

        if (filterStatus) {
            formData.append('status', filterStatus);
        }

        if (campaignId) {
            formData.append('campaign', campaignId);
        }

        if (kodeKabupaten) {
            formData.append('kodeKabupaten', kodeKabupaten);
        }

        if (kodeKecamatan) {
            formData.append('kodeKecamatan', kodeKecamatan);
        }

        if (kodeDesa) {
            formData.append('kodeDesa', kodeDesa);
        }

        if (kodeTimses) {
            formData.append('kodeTimses', kodeTimses);
        }

        Api.putFile(route, {
            method: 'POST',
            body: formData
        }).then(resp => {

            if (resp.data) {

                setRows(resp.data);
                setCurrentPage(resp.meta.pagination.current_page);
                setCurrentPageTable(resp.meta.pagination.current_page - 1);
                setTotal(resp.meta.pagination.total);
                setLoading(false);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const handleSearch = async () => {
        fetchData();
    };

    const handleSort = (val) => {

        let sortedBy = sortedBy;

        if (val !== orderBy) {
            sortedBy = 'asc';
        } else {
            if (sortedBy && sortedBy === 'asc') {
                sortedBy = 'desc';
            } else {
                sortedBy = 'asc';
            }
        }

        setOrderBy(val);
        setSortedBy(sortedBy);
        loading(true);

    };


    const handleChangePage = (event, newPage) => {

        setCurrentPage(newPage + 1);
        setCurrentPageTable(newPage);
        setKodeKabupaten('');
        setKodeKecamatan('');
        setKodeDesa('');
        setLoading(true);

    };

    const handleChangeRowsPerPage = event => {

        setPerPage(event.target.value);
        setCurrentPage(1);
        setLoading(true);

    };

    const handleOpenFilter = () => {
        setShowDialogFilter(true);
    };

    const handleCloseFilter = () => {
        setShowDialogFilter(false);
    };

    const handleResetFilter = () => {
        setTimses('');
        setKodeKabupaten('');
        setKodeKecamatan('');
        setKodeDesa('');
        setLoading(true);
        setShowDialogFilter(false);
        setDataFilter([]);
        setCampaignId('');
    };

    const handleSubmitFilter = () => {
        setLoading(true);
        setShowDialogFilter(false);
        setCurrentPage(1);

        const kode = [kodeProvince];

        if (kodeKabupaten != '') {
            kode.push(kodeKabupaten);
        }

        if (kodeKecamatan != '') {
            kode.push(kodeKecamatan);
        }

        if (kodeDesa != '') {
            kode.push(kodeDesa);
        }

        let formData = new FormData();
        kode.forEach(kd => formData.append('kode[]', kd))

        Api.putFile('/find/areas', {
            method: 'POST',
            body: formData
        }).then(resp => {
            const data = [];
            const respObj = Object.keys(resp).map(key => resp[key]);
            respObj.map((val) => data.push(val.nama));
            setDataFilter(data);

        }).catch(err => {
            console.log(err);
        });
    };

    function handleChange(e, prop) {

        const value = e.target.value;

        if (prop === 'kandidat') {
            setCampaignId(value);
        }

        if (prop === 'kode_timses') {
            setTimses(value);
        }

        if (prop === 'kode_province') {
            setKodeProvince(value);
        }

        if (prop === 'kode_kabupaten') {
            setKodeKabupaten(value);
            dispatch(payloadKecamatan(value));
        }

        if (prop === 'kode_kecamatan') {
            setKodeKecamatan(value);
            dispatch(payloadKelurahan(value));
        }

    };

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            setSearchBy(event.target.value);
            setLoading(true);
        }
    }


    return (
        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h1 className="page-title">Pemilih</h1>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Data Pemilih</li>
                    </ol>
                </nav>

            </div>
            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">
                    <div className="row align-items-center mb-md-3">
                        <div className="col-md-3">
                            <TextField
                                id="input-with-icon-textfield"
                                variant="outlined"
                                className="search-field"
                                onChange={e => setSearchBy(e.target.value)}
                                onBlur={handleSearch}
                                onKeyDown={handleKeyDown}
                                placeholder="Cari disini"
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="start">
                                            <IconButton
                                                aria-label="Search click"
                                                onClick={handleSearch}
                                            >
                                                <i className="fas fa-search"> </i>
                                            </IconButton>
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </div>
                        <div className="col-md-9 d-flex">
                            <Button
                                variant='contained'
                                color='primary'
                                className='ml-3 round'
                                onClick={handleOpenFilter}
                            >
                                <TuneIcon />
                                Filters
                            </Button>

                            <Button
                                variant='contained'
                                color='secondary'
                                className='ml-3 round'
                            >
                                Total: {total.toLocaleString()}
                            </Button>


                            {dataFilter.length > 0 && <div className="box-filter">
                                {dataFilter.map(data => (
                                    <>
                                        {
                                            data !== undefined && <span>{data}</span>
                                        }

                                    </>
                                ))}
                            </div>}

                        </div>
                    </div>

                    <Table className="table-list" size="small" >
                        <TableHead>
                            <TableRow>
                                <TableCell>Kandidat</TableCell>
                                <TableCell className="clickable" onClick={() => handleSort('kabupaten')} >Kabupaten
                                    {orderBy === 'kabupaten' && (
                                        <span className="icon-sort">{
                                            sortedBy === 'asc' ? (
                                                <i className="fas fa-sort-up"> </i>
                                            ) : (
                                                <i className="fas fa-sort-down"> </i>
                                            )
                                        }</span>
                                    )}
                                </TableCell>
                                <TableCell className="clickable" onClick={() => handleSort('kecamatan')} >Kecamatan
                                    {orderBy === 'kecamatan' && (
                                        <span className="icon-sort">{
                                            sortedBy === 'asc' ? (
                                                <i className="fas fa-sort-up"> </i>
                                            ) : (
                                                <i className="fas fa-sort-down"> </i>
                                            )
                                        }</span>
                                    )}
                                </TableCell>

                                <TableCell className="clickable" onClick={() => handleSort('desa')} >Desa
                                    {orderBy === 'desa' && (
                                        <span className="icon-sort">{
                                            sortedBy === 'asc' ? (
                                                <i className="fas fa-sort-up"> </i>
                                            ) : (
                                                <i className="fas fa-sort-down"> </i>
                                            )
                                        }</span>
                                    )}
                                </TableCell>
                                <TableCell className="clickable" onClick={() => handleSort('tps')} >TPS
                                    {orderBy === 'tps' && (
                                        <span className="icon-sort">{
                                            sortedBy === 'asc' ? (
                                                <i className="fas fa-sort-up"> </i>
                                            ) : (
                                                <i className="fas fa-sort-down"> </i>
                                            )
                                        }</span>
                                    )}
                                </TableCell>
                                <TableCell>Group</TableCell>
                                <TableCell>Timses</TableCell>
                                <TableCell className="clickable" onClick={() => handleSort('name')} >Pemilih
                                    {orderBy === 'name' && (
                                        <span className="icon-sort">{
                                            sortedBy === 'asc' ? (
                                                <i className="fas fa-sort-up"> </i>
                                            ) : (
                                                <i className="fas fa-sort-down"> </i>
                                            )
                                        }</span>
                                    )}
                                </TableCell>
                                {/* <TableCell>Tempat Lahir</TableCell>
                                <TableCell>Tanggal Lahir</TableCell> */}
                                <TableCell>Kawin</TableCell>
                                <TableCell>Jenis Kelamin</TableCell>
                                <TableCell>Status</TableCell>


                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {loading ? (
                                <TableRow>
                                    <TableCell colSpan={16} align="center" className="py-5">
                                        <CircularProgress color="primary" />
                                    </TableCell>
                                </TableRow>
                            ) : (
                                rows.length === 0 ? (
                                    <TableRow style={{ height: 33 * perPage }}>
                                        <TableCell colSpan={16} style={{ textAlign: "center" }}>Belum ada pendukung</TableCell>
                                    </TableRow>
                                ) : (
                                    rows.map(row => (
                                        <TableRow
                                            key={row.id}
                                            selected={row.selected}>
                                            <TableCell>
                                                <span>{row.campaign_name === null ? '' : row.campaign_name}</span>
                                            </TableCell>

                                            <TableCell>
                                                <span>{row.nama_kabupaten === null ? '' : row.nama_kabupaten}</span>
                                            </TableCell>
                                            <TableCell>
                                                <span>{row.nama_kecamatan === null ? '' : row.nama_kecamatan}</span>
                                            </TableCell>
                                            <TableCell>
                                                <span>{row.nama_desa === null ? '' : row.nama_desa}</span>
                                            </TableCell>
                                            <TableCell>
                                                <span>{row.tps === null ? '' : row.tps}</span>
                                            </TableCell>
                                            <TableCell>
                                                <span>{row.group_name === null ? '' : row.group_name}</span>
                                            </TableCell>
                                            <TableCell>
                                                <span>{row.timses_name === null ? '' : row.timses_name}</span>
                                            </TableCell>
                                            <TableCell>
                                                <span>{row.voter_name === null ? '' : row.voter_name}</span>
                                            </TableCell>
                                            {/* <TableCell>
                                                <span>{row.tempat_lahir === null ? '' : row.tempat_lahir}</span>
                                            </TableCell>
                                            <TableCell>
                                                <span>{row.tanggal_lahir === null ? '' : row.tanggal_lahir}</span>
                                            </TableCell> */}
                                            <TableCell>
                                                <span>{row.kawin === null ? '' : row.kawin}</span>
                                            </TableCell>
                                            <TableCell>
                                                <span>{row.jenis_kelamin === null ? '' : row.jenis_kelamin}</span>
                                            </TableCell>
                                            <TableCell style={{ width: 160 }}>
                                                <span className={row.status.replace(/\s/g, '')}>{row.status === null ? '' : row.status}</span>
                                            </TableCell>


                                        </TableRow>
                                    ))
                                )
                            )}

                        </TableBody>
                    </Table>
                </div>

                <TablePagination
                    rowsPerPageOptions={[10, 15, 25, 50]}
                    component="div"
                    count={total}
                    rowsPerPage={perPage}
                    page={currentPageTable}
                    backIconButtonProps={{
                        'aria-label': 'previous page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'next page',
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                />

            </div>

            <Dialog
                maxWidth='md'
                fullWidth={true}
                open={showDialogFilter}
                onClose={handleCloseFilter}
                aria-labelledby='form-dialog-title'>
                <DialogTitle id='form-dialog-title'>Filter</DialogTitle>
                <DialogContent>
                    <form name='filter' id='filterPendukung' noValidate>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Kandidat</label>
                                    <TextField variant='outlined'
                                        select
                                        id='kandidat'
                                        name='kandidat'
                                        label='Kandidat'
                                        onChange={e => handleChange(e, 'kandidat')}
                                        value={campaignId}
                                        defaultValue={''}
                                        fullWidth
                                    >
                                        {loadingTimses ?
                                            (<CircularProgress />) : dataCampaign ? (dataCampaign.map(option => (
                                                <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                    {option.name}
                                                </MenuItem>
                                            ))) : errorTimses ? (<p>{errorTimses}</p>) : (<p>Data Not Found!</p>)}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <label>Tim Sukses</label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_timses'
                                        name='kode_timses'
                                        label='Tim Sukses'
                                        onChange={e => handleChange(e, 'kode_timses')}
                                        value={kodeTimses}
                                        defaultValue={''}
                                        fullWidth
                                    >
                                        {loadingTimses ?
                                            (<CircularProgress />) : dataTimses ? (dataTimses.map(option => (
                                                <MenuItem key={parseInt(option.id)} value={option.id}>
                                                    {option.name}
                                                </MenuItem>
                                            ))) : errorTimses ? (<p>{errorTimses}</p>) : (<p>Data Not Found!</p>)}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <label>Provinsi</label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_province'
                                        name='kode_province'
                                        label='Provinsi'
                                        onChange={e => handleChange(e, 'kode_province')}
                                        value={kodeProvince}
                                        defaultValue={''}
                                        fullWidth
                                    >
                                        {loadingProvince ?
                                            (<CircularProgress />) : dataProvince ? (dataProvince.map(option => (
                                                <MenuItem key={parseInt(option.id)} value={option.kode_provinsi}>
                                                    {option.province}
                                                </MenuItem>
                                            ))) : errorProvince ? (<p>{errorProvince}</p>) : (<p>Data Not Found!</p>)}
                                    </TextField>
                                </div>
                                <div className="form-group">
                                    <label>Kabupaten</label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_kabupaten'
                                        name='kode_kabupaten'
                                        label='Kabupaten'
                                        onChange={e => handleChange(e, 'kode_kabupaten')}
                                        value={kodeKabupaten}
                                        defaultValue={''}
                                        fullWidth
                                    >
                                        {loadingKabupaten ?
                                            (<CircularProgress />) : dataKabupaten ? (dataKabupaten.map(option => (
                                                <MenuItem key={parseInt(option.id)} value={option.kode_kabupaten}>
                                                    {option.kabupaten}
                                                </MenuItem>
                                            ))) : errorKabupaten ? (<p>{errorKabupaten}</p>) : (<p>Data Not Found!</p>)}
                                    </TextField>
                                </div>

                                <div className="form-group">
                                    <label>Kecamatan</label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_kecamatan'
                                        name='kode_kecamatan'
                                        label='Kecamatan'
                                        onChange={e => handleChange(e, 'kode_kecamatan')}
                                        value={kodeKecamatan}
                                        fullWidth
                                    >
                                        {loadingKecamatan ?
                                            (<CircularProgress />) : dataKecamatan ? (dataKecamatan.map(option => (
                                                <MenuItem key={parseInt(option.id)} value={option.kode_kecamatan}>
                                                    {option.kecamatan}
                                                </MenuItem>
                                            ))) : errorKecamatan ? (<p>{errorKecamatan}</p>) : (<p>Data Not Found!</p>)}
                                    </TextField>
                                </div>

                                <div className="form-group">
                                    <label>Desa</label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_desa'
                                        name='kode_desa'
                                        label='Desa'
                                        onChange={e => setKodeDesa(e.target.value)}
                                        value={kodeDesa}
                                        fullWidth
                                    >
                                        {loadingKelurahan ?
                                            (<CircularProgress />) : dataKelurahan ? (dataKelurahan.map(option => (
                                                <MenuItem key={parseInt(option.id)} value={option.kode_kelurahan}>
                                                    {option.kelurahan}
                                                </MenuItem>
                                            ))) : errorKelurahan ? (<p>{errorKelurahan}</p>) : (<p>Data Not Found!</p>)}
                                    </TextField>
                                </div>
                            </div>
                        </div>
                    </form>

                </DialogContent>
                <DialogActions className='justify-content-center pb-3'>
                    <Button
                        variant='contained'
                        className='mr-3'
                        onClick={handleResetFilter}
                    >
                        Reset
                    </Button>
                    <Button
                        variant='contained'
                        color='primary'
                        className=''
                        onClick={handleSubmitFilter}
                        disabled={loadingButton && true}
                    >
                        Submit{loadingButton && <i className='fa fa-spinner fa-spin'> </i>}
                    </Button>
                </DialogActions>
            </Dialog>

            <ToastContainer />

        </div>
    )

}