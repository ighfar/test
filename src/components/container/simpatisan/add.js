import React from "react";
import { Link } from 'react-router-dom';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";
import TextField from "@material-ui/core/TextField";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Skeleton from '@material-ui/lab/Skeleton';
import MenuItem from '@material-ui/core/MenuItem';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import moment from 'moment';


require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

export default function SimpatisanAdd() {

    const dataGender = [
        {
            id: 'P',
            name: "Pria"
        },
        {
            id: 'W',
            name: "Wanita"
        }
    ];

    const dataStatus = [
        {
            id: 'B',
            name: "Belum Menikah"
        },
        {
            id: 'S',
            name: "Sudah Menikah"
        }
    ];

    const dataReligion = [
        {
            id: 'Islam',
            name: "Islam"
        },
        {
            id: 'Kristen',
            name: "Kristen"
        },
        {
            id: 'Khatolik',
            name: "Khatolik"
        },
        {
            id: 'Hindu',
            name: "Hindu"
        },
        {
            id: 'Buddha',
            name: "Buddha"
        },
        {
            id: 'Konghucu',
            name: "Konghucu"
        },
    ];

    const [loading, setLoading] = React.useState(false);
    const [errors, setErrors] = React.useState([]);
    const [genders, setGenders] = React.useState([]);
    const [status, setStatus] = React.useState([]);
    const [religion, setReligion] = React.useState([]);
    const [provinces, setProvinces] = React.useState([]);
    const [kabupaten, setKabupaten] = React.useState([]);
    const [kecamatan, setKecamatan] = React.useState([]);
    const [desa, setDesa] = React.useState([]);
    const [campaign, setCampaign] = React.useState([]);

    const [nik, setNik] = React.useState('');
    const [name, setName] = React.useState('');
    const [gender, setGender] = React.useState('');
    const [place, setPlace] = React.useState('');
    const [date, setDate] = React.useState(null);

    const [kodeProvince, setKodeProvince] = React.useState('');
    const [kodeKabupaten, setKodeKabupaten] = React.useState('');
    const [kodeKecamatan, setKodeKecamatan] = React.useState('');
    const [kodeDesa, setKodeDesa] = React.useState('');
    const [alamat, setAlamat] = React.useState('');
    const [agama, setAgama] = React.useState('Islam');
    const [pernikahan, setPernikahan] = React.useState('B');
    const [pekerjaan, setPekerjaan] = React.useState('');
    const [campaignId, setCampaignId] = React.useState('');


    React.useEffect(() => {

        document.title = 'Admin suarapemilu - Halaman Tambah Simpatisan';


        Api.get('/province/areas').then(resp => {


            if (resp) {
                setProvinces(Object.keys(resp).map(key => resp[key]));
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/campaigns').then(resp => {

            if (resp.data) {
                const campaign = resp.data;

                setCampaign(campaign);
            }

        }).catch(err => {
            console.log(err);
        });


        fetchDataKabupaten('');

        setStatus(dataStatus);
        setReligion(dataReligion);
        setGenders(dataGender);

    }, [dataStatus, dataReligion, dataGender]);


    const fetchDataKabupaten = async (province) => {

        Api.get('/kabupaten/areas?province=' + province).then(resp => {


            if (resp) {

                setKabupaten(Object.keys(resp).map(key => resp[key]));

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchDataKecamatan = async (kabupaten) => {

        Api.get('/kecamatan/areas?kabupaten=' + kabupaten).then(resp => {


            if (resp) {

                setKecamatan(Object.keys(resp).map(key => resp[key]));

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchDataDesa = async (kecamatan) => {

        Api.get('/kelurahan/areas?kecamatan=' + kecamatan).then(resp => {


            if (resp) {

                setDesa(Object.keys(resp).map(key => resp[key]));

            }

        }).catch(err => {
            console.log(err);
        });
    };


    function handleChange(e, prop) {

        const value = e.target.value;

        if (prop === 'kode_province') {
            setKodeProvince(value);
            fetchDataKabupaten(value);
        }

        if (prop === 'kode_kabupaten') {
            setKodeKabupaten(value);
            fetchDataKecamatan(value);
        }

        if (prop === 'kode_kecamatan') {
            setKodeKecamatan(value);
            fetchDataDesa(value);
        }

    };

    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    const handleSubmit = async () => {

        if (!validator.allValid()) {

            setErrors(validator.getErrorMessages());

            return false;

        }

        setLoading(true);

        let formData = new FormData();
        formData.append('nik', nik);
        formData.append('name', name);
        formData.append('gender', gender);
        formData.append('place_of_birth', place);
        formData.append('date_of_birth', moment(date).format('YYYY-MM-DD'));
        formData.append('address', alamat);
        formData.append('kode_province', kodeProvince);
        formData.append('kode_kabupaten', kodeKabupaten);
        formData.append('kode_kecamatan', kodeKecamatan);
        formData.append('kode_desa', kodeDesa);
        formData.append('religion', agama);
        formData.append('status_perkawinan', pernikahan);
        formData.append('pekerjaan', pekerjaan);
        formData.append('campaign_id', campaignId);

        Api.putFile('/admin/simpatisan', {
            method: 'POST',
            body: formData
        }).then(resp => {

            setLoading(false);

            history.push('/simpatisan');

            showMessage(true, 'Simpatisan berhasil dibuat');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                setLoading(false);
                setErrors(err.errors);

                showMessage(false, 'Invalid format data');
            }
        });

    };


    const handleBack = () => {
        history.push('/simpatisan');
    };


    return (
        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h1 className="page-title">Tambah Simpatisan</h1>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                        <li className="breadcrumb-item"><Link to="/simpatisan" >Simpatisan</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Tambah</li>
                    </ol>
                </nav>

            </div>
            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">
                    <form name="add" id="addSimpatisan" className="row" noValidate>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Nama Kandidat</label>
                                <TextField variant='outlined'
                                    select
                                    id='campaign_id'
                                    name='campaign_id'
                                    label='Kandidat'
                                    onChange={e => setCampaignId(e.target.value)}
                                    value={campaignId}
                                    fullWidth
                                >
                                    {campaign.map(option => (
                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>

                            <div className="form-group">
                                <label>Nomer Induk Kependudukan <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='nik'
                                    name="nik"
                                    label="Input NIK"
                                    onChange={e => setNik(e.target.value)}
                                    value={nik}
                                    fullWidth
                                />
                                {validator.message('nik', nik, 'required')}
                                <div className='text-danger'>{errors.nik}</div>
                            </div>

                            <div className="form-group">
                                <label>Nama <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='name'
                                    name="name"
                                    label="Input Nama"
                                    onChange={e => setName(e.target.value)}
                                    value={name}
                                    fullWidth
                                />
                                {validator.message('name', name, 'required')}
                                <div className='text-danger'>{errors.name}</div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Jenis Kelamin<span className="required">*</span></label>
                                    <RadioGroup
                                        row={!loading}
                                        aria-labelledby="demo-radio-buttons-group-label"
                                        defaultValue="female"
                                        name="radio-buttons-group"
                                    >
                                        {loading ? (
                                            <Skeleton variant="text" />
                                        ) : (
                                            genders.length == 0 ? (
                                                <div className='selected-empty'>Belum Ada Data</div>
                                            ) : (

                                                genders.map(row => (

                                                    <FormControlLabel
                                                        name="gender"
                                                        onChange={e => setGender(e.target.value)}
                                                        value={row.id.toString()}
                                                        control={<Radio />}
                                                        label={row.name}
                                                        key={row.id} />


                                                ))
                                            )
                                        )}
                                    </RadioGroup>
                                    {validator.message('gender', gender, 'required')}
                                    <div className='text-danger'>{errors.gender}</div>
                                </div>
                            </div>
                            <div className="col-md-6"></div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Tempat Lahir <span className="required">*</span></label>
                                        <TextField variant="outlined"
                                            type='text'
                                            id='place'
                                            name="place"
                                            label="Tempat Lahir"
                                            onChange={e => setPlace(e.target.value)}
                                            value={place}
                                            fullWidth
                                        />
                                        {validator.message('place', place, 'required')}
                                        <div className='text-danger'>{errors.place}</div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className='form-group'>
                                        <label>Tanggal Lahir</label>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <DatePicker
                                                label='Tanggal Lahir'
                                                value={date}
                                                onChange={(event) => setDate(event)}
                                                format={'dd MMM yyyy'}
                                                cancelLabel='BATAL'
                                                inputVariant='outlined'
                                            />
                                        </MuiPickersUtilsProvider>
                                        {date &&
                                            <button
                                                type='button'
                                                onClick={(event) => setDate(event)}
                                                className='btn btn-delete-select'><i className='fas fa-times-circle'> </i>
                                            </button>
                                        }
                                        <div className='text-danger'>{errors.date}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Provinsi <span className="required">*</span></label>
                                <TextField variant='outlined'
                                    select
                                    id='kode_province'
                                    name='kode_province'
                                    label='Provinsi'
                                    onChange={e => handleChange(e, 'kode_province')}
                                    value={kodeProvince}
                                    defaultValue={''}
                                    fullWidth
                                >
                                    {provinces.map(option => (
                                        <MenuItem key={parseInt(option.id)} value={option.kode_provinsi}>
                                            {option.province}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                {validator.message('kode_province', kodeProvince, 'required')}
                                <div className='text-danger'>{errors.kode_province}</div>
                            </div>


                            <div className="form-group">
                                <label>Kabupaten</label>
                                <TextField variant='outlined'
                                    select
                                    id='kode_kabupaten'
                                    name='kode_kabupaten'
                                    label='Kabupaten'
                                    onChange={e => handleChange(e, 'kode_kabupaten')}
                                    value={kodeKabupaten}
                                    fullWidth
                                >
                                    {kabupaten.map(option => (
                                        <MenuItem key={parseInt(option.id)} value={option.kode_kabupaten}>
                                            {option.kabupaten}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>

                            <div className="form-group">
                                <label>Kecamatan</label>
                                <TextField variant='outlined'
                                    select
                                    id='kode_kecamatan'
                                    name='kode_kecamatan'
                                    label='Kecamatan'
                                    onChange={e => handleChange(e, 'kode_kecamatan')}
                                    value={kodeKecamatan}
                                    fullWidth
                                >
                                    {kecamatan.map(option => (
                                        <MenuItem key={parseInt(option.id)} value={option.kode_kecamatan}>
                                            {option.kecamatan}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>

                            <div className="form-group">
                                <label>Desa</label>
                                <TextField variant='outlined'
                                    select
                                    id='kode_desa'
                                    name='kode_desa'
                                    label='Desa'
                                    onChange={e => setKodeDesa(e.target.value)}
                                    value={kodeDesa}
                                    fullWidth
                                >
                                    {desa.map(option => (
                                        <MenuItem key={parseInt(option.id)} value={option.kode_kelurahan}>
                                            {option.kelurahan}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>


                        </div>
                        <div className="col-md-6">

                            <div className="form-group">
                                <label>Alamat<span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='outlined-multiline-alamat'
                                    name="alamat"
                                    label="Alamat"
                                    multiline
                                    rows={4}
                                    onChange={e => setAlamat(e.target.value)}
                                    value={alamat}
                                    fullWidth
                                />
                                {validator.message('alamat', alamat, 'required')}
                                <div className='text-danger'>{errors.alamat}</div>
                            </div>


                            <div className="form-group">
                                <label>Agama<span className="required">*</span></label>
                                <RadioGroup
                                    row={!loading}
                                    aria-labelledby="demo-radio-buttons-group-label"
                                    defaultValue=""
                                    name="radio-buttons-group"
                                >
                                    {loading ? (
                                        <Skeleton variant="text" />
                                    ) : (
                                        religion.length == 0 ? (
                                            <div className='selected-empty'>Belum Ada Data</div>
                                        ) : (

                                            religion.map(row => (
                                                <FormControlLabel
                                                    name="agama"
                                                    onChange={e => setAgama(e.target.value)}
                                                    value={row.id.toString()}
                                                    control={<Radio />}
                                                    label={row.name}
                                                    key={row.id} />
                                            ))
                                        )
                                    )}

                                </RadioGroup>
                            </div>

                            <div className="form-group">
                                <label>Status Kawin<span className="required">*</span></label>
                                <RadioGroup
                                    row={!loading}
                                    aria-labelledby="demo-radio-buttons-group-label"
                                    defaultValue=""
                                    name="radio-buttons-group"
                                >
                                    {loading ? (
                                        <Skeleton variant="text" />
                                    ) : (
                                        status.length == 0 ? (
                                            <div className='selected-empty'>Belum Ada Data</div>
                                        ) : (

                                            status.map(row => (
                                                <FormControlLabel
                                                    name="pernikahan"
                                                    onChange={e => setPernikahan(e.target.value)}
                                                    value={row.id.toString()}
                                                    control={<Radio />}
                                                    label={row.name}
                                                    key={row.id} />
                                            ))
                                        )
                                    )}

                                </RadioGroup>
                            </div>

                            <div className="form-group">
                                <label>Pekerjaan</label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='pekerjaan'
                                    name="pekerjaan"
                                    label="Pekerjaan"
                                    onChange={e => setPekerjaan(e.target.value)}
                                    value={pekerjaan}
                                    fullWidth
                                />
                            </div>


                        </div>

                        <div className="col-12 text-left">
                            <Button
                                variant="contained"
                                className="mr-3"
                                onClick={handleBack}
                            >
                                Kembali
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className=""
                                onClick={handleSubmit}
                                disabled={loading}
                            >
                                Simpan{loading && <i className="fa fa-spinner fa-spin"> </i>}
                            </Button>
                        </div>
                    </form>
                </div>
            </div>

            <ToastContainer />

        </div>
    )

}

