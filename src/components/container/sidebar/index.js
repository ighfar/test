import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter, Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { RootActions } from "../../../shared/root-action";
import { connect } from "react-redux";
import AuthHelper from "../../../libraries/auth-helper";
import Api from "../../../libraries/api";
import user from "../../../images/user.jpg";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DashboardIcon from "@material-ui/icons/Dashboard";
import TuneIcon from "@material-ui/icons/Tune";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import EditLocationIcon from "@material-ui/icons/EditLocation";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import BookIcon from "@material-ui/icons/Book";
import GavelIcon from "@material-ui/icons/Gavel";
import FlagIcon from "@material-ui/icons/Flag";
import LanguageIcon from "@material-ui/icons/Language";
import AssessmentIcon from "@material-ui/icons/Assessment";
import ImageSearchIcon from "@material-ui/icons/ImageSearch";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";
import SupervisedUserCircleIcon from "@material-ui/icons/SupervisedUserCircle";
import InfoIcon from "@material-ui/icons/Info";
import PaymentIcon from "@material-ui/icons/Payment";
import { history } from "../../../shared/configure-store";

class Sidebar extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      menus: [],
      photo: user,
      userName: "",
      userRole: "",
      top: false,
      data: false,
      client: false,
      projects: false,
      finance: false,
      tasks: false,
      permissions: [],
    };
  }

  componentDidMount() {
    Api.get("/profile")
      .then((resp) => {
        if (resp) {
          let data = resp.user;

          AuthHelper.setProfile(data);

          this.props.setProfile(data);

          this.props.setRole(data.roles[0].name);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  checkPermission = (name) => {
    let all = this.state.permissions.filter((item) => item.name === name);
    if (all.length > 0 || this.state.userRole === "superadmin") return true;
  };

  handleLogout = () => {
    AuthHelper.logOut();
  };

  openMenu = (menu) => {
    this.setState({
      [menu]: !this.state[menu],
    });
  };

  handleGo = (link) => {
    history.push(link);
  };

  render() {
    return (
      <aside
        className={
          "side-nav " + (this.props.toggle_sidebar_state ? "show" : "")
        }
      >
        <header className="d-none d-lg-block">
          <nav className="navbar justify-content-center">
            <Link to="#" className="navbar-brand m-0 p-0"></Link>
          </nav>
        </header>
        <ul className="nav flex-column">
          <li className="nav-item profile-picture">
            <img
              src={this.state.photo}
              alt="user"
              className="user-photo mr-2"
            />
            <div className="content">
              <h3>{this.props.profile_state.name}</h3>
              <span>{this.props.role_state}</span>
            </div>
          </li>
          <List component="nav" aria-labelledby="nested-list-subheader">
            <ListItem
              button
              onClick={() => this.handleGo("/")}
              selected={this.props.location.pathname === "/"}
            >
              <ListItemIcon>
                <DashboardIcon />
              </ListItemIcon>
              <ListItemText primary="Dashboard" />
            </ListItem>

            {this.props.role_state === "adminkandidat" && (
              <ListItem
                button
                onClick={() => this.handleGo("/peta")}
                selected={this.props.location.pathname === "/peta"}
              >
                <ListItemIcon>
                  <ImageSearchIcon />
                </ListItemIcon>
                <ListItemText primary="Peta Suara" />
              </ListItem>
            )}

            {this.props.role_state === "adminkandidat" && (
              <ListItem
                button
                onClick={() => this.handleGo("/calculation")}
                selected={this.props.location.pathname === "/calculation"}
              >
                <ListItemIcon>
                  <AssessmentIcon />
                </ListItemIcon>
                <ListItemText primary="Perhitungan" />
              </ListItem>
            )}

            <hr />

            <ListItem
              button
              onClick={() => this.handleGo("/timses")}
              selected={this.props.location.pathname === "/timses"}
            >
              <ListItemIcon>
                <GavelIcon />
              </ListItemIcon>
              <ListItemText primary="Tim & Saksi" />
            </ListItem>

            <ListItem
              button
              onClick={() => this.handleGo("/timses-blacklist")}
              selected={this.props.location.pathname === "/timses-blacklist"}
            >
              <ListItemIcon>
                <InfoIcon />
              </ListItemIcon>
              <ListItemText primary="Tim & Saksi Di Tolak" />
            </ListItem>

            {this.props.role_state !== "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/capel")}
                selected={this.props.location.pathname === "/capel"}
              >
                <ListItemIcon>
                  <AccountCircleIcon />
                </ListItemIcon>
                <ListItemText primary="Pemilih" />
              </ListItem>
            )}

            {this.props.role_state !== "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/blanko")}
                selected={this.props.location.pathname === "/blanko"}
              >
                <ListItemIcon>
                  <PaymentIcon />
                </ListItemIcon>
                <ListItemText primary="Blanko" />
              </ListItem>
            )}
               {this.props.role_state !== "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/voter")}
                selected={this.props.location.pathname === "/voter"}
              >
                <ListItemIcon>
                  <PaymentIcon />
                </ListItemIcon>
                <ListItemText primary="DPT" />
              </ListItem>
            )}

            <ListItem
              button
              onClick={() => this.handleGo("/simpatisan")}
              selected={this.props.location.pathname === "/simpatisan"}
            >
              <ListItemIcon>
                <SupervisedUserCircleIcon />
              </ListItemIcon>
              <ListItemText primary="Simpatisan" />
            </ListItem>

            <hr />

            {this.props.role_state === "adminkandidat" && (
              <ListItem
                button
                onClick={() => this.handleGo("/campaign-group")}
                selected={this.props.location.pathname === "/campaign-group"}
              >
                <ListItemIcon>
                  <SupervisedUserCircleIcon />
                </ListItemIcon>
                <ListItemText primary="Group" />
              </ListItem>
            )}

            {this.props.role_state === "adminkandidat" && (
              <ListItem
                button
                onClick={() => this.handleGo("/admin")}
                selected={this.props.location.pathname === "/admin"}
              >
                <ListItemIcon>
                  <SupervisedUserCircleIcon />
                </ListItemIcon>
                <ListItemText primary="Admin Group" />
              </ListItem>
            )}

            {this.props.role_state === "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/capres")}
                selected={this.props.location.pathname === "/capres"}
              >
                <ListItemIcon>
                  <AssignmentIndIcon />
                </ListItemIcon>
                <ListItemText primary="Calon Presiden" />
              </ListItem>
            )}

            <ListItem
              button
              onClick={() => this.handleGo("/campaign")}
              selected={this.props.location.pathname === "/campaign"}
            >
              <ListItemIcon>
                <AssignmentIndIcon />
              </ListItemIcon>
              <ListItemText primary="Kandidat" />
            </ListItem>

            {this.props.role_state === "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/partai")}
                selected={this.props.location.pathname === "/partai"}
              >
                <ListItemIcon>
                  <FlagIcon />
                </ListItemIcon>
                <ListItemText primary="Partai" />
              </ListItem>
            )}

            {this.props.role_state === "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/dapil")}
                selected={this.props.location.pathname === "/dapil"}
              >
                <ListItemIcon>
                  <LanguageIcon />
                </ListItemIcon>
                <ListItemText primary="Dapil" />
              </ListItem>
            )}

            <ListItem
              button
              onClick={() => this.handleGo("/articles")}
              selected={this.props.location.pathname === "/articles"}
            >
              <ListItemIcon>
                <BookIcon />
              </ListItemIcon>
              <ListItemText primary="Artikel" />
            </ListItem>

            {this.props.role_state === "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/banner")}
                selected={this.props.location.pathname === "/banner"}
              >
                <ListItemIcon>
                  <AssignmentIndIcon />
                </ListItemIcon>
                <ListItemText primary="Banner" />
              </ListItem>
            )}

            {this.props.role_state === "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/wilayah")}
                selected={this.props.location.pathname === "/wilayah"}
              >
                <ListItemIcon>
                  <EditLocationIcon />
                </ListItemIcon>
                <ListItemText primary="Wilayah" />
              </ListItem>
            )}

            <hr />

            {this.props.role_state === "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/users")}
                selected={this.props.location.pathname === "/users"}
              >
                <ListItemIcon>
                  <AccountCircleIcon />
                </ListItemIcon>
                <ListItemText primary="Pengguna" />
              </ListItem>
            )}

            {this.props.role_state === "superadmin" && (
              <ListItem
                button
                onClick={() => this.handleGo("/roles")}
                selected={this.props.location.pathname === "/roles"}
              >
                <ListItemIcon>
                  <TuneIcon />
                </ListItemIcon>
                <ListItemText primary="Hak Akses" />
              </ListItem>
            )}

            <ListItem button onClick={this.handleLogout}>
              <ListItemIcon>
                <ExitToAppIcon />
              </ListItemIcon>
              <ListItemText primary="Logout" />
            </ListItem>
          </List>
        </ul>
      </aside>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    toggle_sidebar_state: state.toggle_sidebar_state,

    profile_state: state.profile_state,

    permission_state: state.permission_state,

    role_state: state.role_state,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(RootActions, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Sidebar));
