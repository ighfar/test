import React from "react";
import { Link } from 'react-router-dom';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";
import TextField from "@material-ui/core/TextField";
import MenuItem from '@material-ui/core/MenuItem';


require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

export default function AdminGroupEdit(props) {

    const [loading, setLoading] = React.useState(false);
    const [errors, setErrors] = React.useState([]);
    const [groups, setGroups] = React.useState([]);
    const [id, setId] = React.useState('');
    const [name, setName] = React.useState('');
    const [groupId, setGroupId] = React.useState('');

    React.useEffect(() => {

        document.title = 'Halaman Rubah Timses';

        const idGroup = props.match.params.id;

        Api.get('/users/' + idGroup).then(resp => {

            if (resp.data) {
                let data = resp.data;
                setId(data.id);
                setName(data.name);
                setGroupId(data.group_id);
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/groups?limit=-1').then(resp => {

            if (resp.data) {
                setGroups(resp.data);
            }

        }).catch(err => {
            console.log(err);
        });

    }, []);

    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };


    const handleSubmit = async () => {

        if (!validator.allValid()) {

            setErrors(validator.getErrorMessages());

            return false;

        }

        setLoading(true);

        const params = {
            'name': name,
            'group_id': groupId
        };

        Api.patch('/users/' + id, params).then(resp => {

            setLoading(false);

            history.push('/admin');

            showMessage(true, 'Admin group berhasil dirubah');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                setErrors(err.errors);
                setLoading(false);

                showMessage(false, 'Invalid format data');
            }
        });

    }


    const handleBack = () => {
        history.push('/admin');
    };


    return (
        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h1 className="page-title">Edit Admin Group</h1>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                        <li className="breadcrumb-item"><Link to="/admin" >Admin Group</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>

            </div>
            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">
                    <form name="add" id="addDapilWilayah" className="row" noValidate>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Nama Group<span className="required">*</span></label>
                                <TextField variant='outlined'
                                    select
                                    id='group_id'
                                    name='GroupId'
                                    label='Group'
                                    onChange={e => setGroupId(e.target.value)}
                                    value={groupId}
                                    fullWidth
                                >
                                    {groups.map(option => (
                                        <MenuItem key={parseInt(option.id)} value={option.id} selected={parseInt(option.id) === parseInt(groupId)}>
                                            {option.group_name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                {validator.message('group', groupId, 'required')}
                                <div className='text-danger'>{errors.groupId}</div>
                            </div>
                            <div className="form-group">
                                <label>Nama <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='name'
                                    name="name"
                                    label="Input Nama Group"
                                    onChange={e => setName(e.target.value)}
                                    value={name}
                                    fullWidth
                                />
                                {validator.message('name', name, 'required')}
                                <div className='text-danger'>{errors.name}</div>
                            </div>
                        </div>


                        <div className="col-12 text-left">
                            <Button
                                variant="contained"
                                className="mr-3"
                                onClick={handleBack}
                            >
                                Kembali
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className=""
                                onClick={handleSubmit}
                                disabled={loading}
                            >
                                Simpan{loading && <i className="fa fa-spinner fa-spin"> </i>}
                            </Button>
                        </div>
                    </form>
                </div>
            </div>

            <ToastContainer />

        </div>
    )

}

