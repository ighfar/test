import React from "react";
import { Link } from 'react-router-dom';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";
import TextField from "@material-ui/core/TextField";
import MenuItem from '@material-ui/core/MenuItem';
import Autocomplete from '@material-ui/lab/Autocomplete';


require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

export default function AdminKandidatAdd() {

    const [loading, setLoading] = React.useState(false);
    const [errors, setErrors] = React.useState([]);
    const [campaign, setCampaign] = React.useState([]);
    const [campaignId, setCampaignId] = React.useState('');
    const [selectCampaign, setSelectCampaign] = React.useState({});
    const [selectPartai, setSelectPartai] = React.useState({});
    const [selectGroup, setSelectGroup] = React.useState([]);
    const [partai, setPartai] = React.useState([]);
    const [group, setGroup] = React.useState([]);
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [role, setRole] = React.useState('');
    const [kodePartai, setKodePartai] = React.useState('');
    const [kodeGroup, setKodeGroup] = React.useState('');
    const [openPartai, setOpenPartai] = React.useState(false);
    const [openGroup, setOpenGroup] = React.useState(false);


    React.useEffect(() => {

        document.title = 'Admin suarapemilu - Halaman Tambah Admin Kandidat';

        Api.get('/select/campaigns').then(resp => {

            if (resp.data) {
                const campaign = resp.data;

                setCampaign(campaign);
            }

        }).catch(err => {
            console.log(err);
        });

    }, []);


    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    const handleSubmit = async () => {

        if (!validator.allValid()) {

            setErrors(validator.getErrorMessages());

            return false;

        }

        setLoading(true);


        let formData = new FormData();
        formData.append('campaign_id', campaignId);
        formData.append('name', name);
        formData.append('email', email);
        formData.append('password', password);
        formData.append('role', role);
        formData.append('group_id', kodeGroup);
        formData.append('kode_partai', kodePartai);

        Api.putFile('/admin/kandidat', {
            method: 'POST',
            body: formData
        }).then(resp => {

            setLoading(false);

            history.push('/users');

            showMessage(true, 'Admin kandidat berhasil dibuat');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                setLoading(false);
                setErrors(err.errors);

                showMessage(false, 'Invalid format data');
            }
        });

    };


    const handleBack = () => {
        history.push('/users');
    };

    const onChangeCampaign = (event, object) => {
        if (object != null) {
            setCampaignId(object.real_id);
            setSelectCampaign(object);
        }
    }

    const onChangePartai = (event, object) => {
        if (object != null) {
            setKodePartai(object.real_id);
            setSelectPartai(object);
        }
    }

    const onChangeGroup = (event, object) => {
        if (object != null) {
            setKodeGroup(object.id);
            setSelectGroup(object);
        }
    }

    const onChangeRole = (event) => {

        const role = event.target.value;
        setRole(role);

        if (role === 'adminpartai') {

            setOpenPartai(true);

            Api.get('/select/partai').then(resp => {

                if (resp.data) {
                    const data = resp.data;
                    setPartai(data);
                }

            }).catch(err => {
                console.log(err);
            });
        }

        if (role === 'admin') {

            setOpenGroup(true);

            Api.get('/select/groups?campaign_id=' + campaignId).then(resp => {

                if (resp.data) {
                    const data = resp.data;
                    setGroup(data);
                }

            }).catch(err => {
                console.log(err);
            });
        }

    }


    return (
        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h1 className="page-title">Tambah Admin Kandidat</h1>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                        <li className="breadcrumb-item"><Link to="/simpatisan" >Users</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Tambah</li>
                    </ol>
                </nav>

            </div>
            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">
                    <form name="add" id="addSimpatisan" className="row" noValidate>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Nama Kandidat</label>
                                <Autocomplete
                                    id="combo-box-demo"
                                    name="kandidat"
                                    fullWidth
                                    options={campaign}
                                    onChange={onChangeCampaign}
                                    filterSelectedOptions
                                    defaultValue={selectCampaign}
                                    getOptionLabel={(option) => option.name}
                                    renderInput={(params) => <TextField {...params} label="Kandidat" variant="outlined" />}
                                />
                                {validator.message('kandidat', campaignId, 'required')}
                                <div className='text-danger'>{errors.kandidat}</div>
                            </div>


                            <div className="form-group">
                                <label>Nama <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='name'
                                    name="name"
                                    label="Input Nama"
                                    onChange={e => setName(e.target.value)}
                                    value={name}
                                    fullWidth
                                />
                                {validator.message('name', name, 'required')}
                                <div className='text-danger'>{errors.name}</div>
                            </div>

                            <div className="form-group">
                                <label>Email <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='email'
                                    name="email"
                                    label="Input Email"
                                    onChange={e => setEmail(e.target.value)}
                                    value={email}
                                    fullWidth
                                />
                                {validator.message('email', email, 'required|email')}
                                <div className='text-danger'>{errors.email}</div>
                            </div>

                            <div className="form-group">
                                <label>Password <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='password'
                                    id='password'
                                    name="password"
                                    label="Input Password"
                                    onChange={e => setPassword(e.target.value)}
                                    value={password}
                                    fullWidth
                                />
                                {validator.message('password', password, 'required')}
                                <div className='text-danger'>{errors.password}</div>
                            </div>

                            <div className="form-group">
                                <label>Role Admin<span className="required">*</span></label>
                                <TextField variant='outlined'
                                    select
                                    id='role_admin'
                                    name='role'
                                    label='Role Admin'
                                    onChange={onChangeRole}
                                    value={role}
                                    fullWidth
                                >
                                    <MenuItem value="adminpartai" selected={role === 'adminpartai'}>
                                        Admin Partai
                                    </MenuItem>
                                    <MenuItem value="adminkandidat" selected={role === 'adminkandidat'}>
                                        Admin Kandidat
                                    </MenuItem>
                                    <MenuItem value="admin" selected={role === 'admin'}>
                                        Admin Group
                                    </MenuItem>

                                </TextField>
                                {validator.message('role', role, 'required')}
                                <div className='text-danger'>{errors.role}</div>
                            </div>

                            {openPartai &&
                                <div className="form-group">
                                    <label>Partai <span className="required">*</span></label>
                                    <Autocomplete
                                        id="partai-select"
                                        name="partai"
                                        fullWidth
                                        options={partai}
                                        onChange={onChangePartai}
                                        filterSelectedOptions
                                        defaultValue={selectPartai}
                                        getOptionLabel={(option) => option.name}
                                        renderInput={(params) => <TextField {...params} label="Partai" variant="outlined" />}
                                    />
                                    {validator.message('partai', kodePartai, 'required')}
                                    <div className='text-danger'>{openPartai ? errors.partai : ''}</div>
                                </div>
                            }

                            {openGroup &&
                                <div className="form-group">
                                    <label>Group <span className="required">*</span></label>
                                    <Autocomplete
                                        id="group-select"
                                        name="group"
                                        fullWidth
                                        options={group}
                                        onChange={onChangeGroup}
                                        filterSelectedOptions
                                        defaultValue={selectGroup}
                                        getOptionLabel={(option) => option.group_name}
                                        renderInput={(params) => <TextField {...params} label="Group Timses" variant="outlined" />}
                                    />
                                    {validator.message('group', kodeGroup, 'required')}
                                    <div className='text-danger'>{openGroup ? errors.group : ''}</div>
                                </div>
                            }
                        </div>

                        <div className="col-12 text-left">
                            <Button
                                variant="contained"
                                className="mr-3"
                                onClick={handleBack}
                            >
                                Kembali
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className=""
                                onClick={handleSubmit}
                                disabled={loading}
                            >
                                Simpan{loading && <i className="fa fa-spinner fa-spin"> </i>}
                            </Button>
                        </div>
                    </form>
                </div>
            </div>

            <ToastContainer />

        </div>
    )

}

