import React from "react";
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItem from '@material-ui/core/ListItem';
import Skeleton from '@material-ui/lab/Skeleton';

export default function LoadingContent() {
    return (
        <div>
            <ListItem>
                <ListItemIcon>
                    <Skeleton animation="wave" variant="circle" width={40} height={40} />
                </ListItemIcon>
                <ListItemText
                    primary={
                        <>
                            <Skeleton animation="wave" width={250} height={118} />
                        </>
                    }
                />
            </ListItem>
            <ListItem>
                <ListItemIcon>
                    <Skeleton animation="wave" variant="circle" width={40} height={40} />
                </ListItemIcon>
                <ListItemText
                    primary={
                        <>
                            <Skeleton animation="wave" width={250} height={118} />
                        </>
                    }
                />
            </ListItem>
            <ListItem>
                <ListItemIcon>
                    <Skeleton animation="wave" variant="circle" width={40} height={40} />
                </ListItemIcon>
                <ListItemText
                    primary={
                        <>
                            <Skeleton animation="wave" width={250} height={118} />
                        </>
                    }
                />
            </ListItem>
            <ListItem>
                <ListItemIcon>
                    <Skeleton animation="wave" variant="circle" width={40} height={40} />
                </ListItemIcon>
                <ListItemText
                    primary={
                        <>
                            <Skeleton animation="wave" width={250} height={118} />
                        </>
                    }
                />
            </ListItem>
        </div>
    )
}