import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import TextField from "@material-ui/core/TextField";
import SimpleReactValidator from "simple-react-validator";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Skeleton from '@material-ui/lab/Skeleton';
import Switch from '@material-ui/core/Switch';
import { useDispatch, useSelector } from "react-redux";
import { payloadProvicePublic } from "../../../redux/actions/provpublic";
import { payloadKabupatenPublic } from "../../../redux/actions/kabupatenpublic";
import { payloadKecamatanPublic } from "../../../redux/actions/kecamatanpublic";
import { payloadKelurahanPublic } from "../../../redux/actions/kelurahanpublic";
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import moment from 'moment';
import Button from '@material-ui/core/Button';
import { toast, ToastContainer } from "react-toastify";
import ErrorHandling from "../../../libraries/error-handling";
import Api from "../../../libraries/api";
import Storage from "../../../libraries/storage";

const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function AddNewVoters(props) {

    const dataGender = [
        {
            id: 'L',
            name: "Laki Laki"
        },
        {
            id: 'P',
            name: "Perempuan"
        }
    ];

    const dataStatus = [
        {
            id: 'B',
            name: "Belum Menikah"
        },
        {
            id: 'S',
            name: "Sudah Menikah"
        }
    ];

    const classes = useStyles();
    const [nkk, setNKK] = React.useState('');
    const [nik, setNik] = React.useState('');
    const [name, setName] = React.useState('');
    const [place, setPlace] = React.useState('');
    const [date, setDate] = React.useState(null);
    const [maried, setMaried] = React.useState('B');
    const [gender, setGender] = React.useState('L');
    const [address, setAddress] = React.useState('');
    const [rt, setRt] = React.useState('');
    const [rw, setRw] = React.useState('');
    const [tps, setTps] = React.useState('');

    const [difabel, setDifabel] = React.useState(0);
    const [errors, setErrors] = React.useState([]);
    const [genders, setGenders] = React.useState([]);
    const [status, setStatus] = React.useState([]);
    const [loadingMaried, setLoadingMaried] = React.useState(false);
    const [loading, setLoading] = React.useState(false);

    const [kodeProvince, setKodeProvince] = React.useState('11');
    const [kodeKabupaten, setKodeKabupaten] = React.useState('');
    const [kodeKecamatan, setKodeKecamatan] = React.useState('');
    const [kodeDesa, setKodeDesa] = React.useState('');

    const { loadingProvince, errorProvince, dataProvince } = useSelector((state) => state.province_state);
    const { loadingKabupaten, errorKabupaten, dataKabupaten } = useSelector((state) => state.kabupaten_state);
    const { loadingKecamatan, errorKecamatan, dataKecamatan } = useSelector((state) => state.kecamatan_state);
    const { loadingKelurahan, errorKelurahan, dataKelurahan } = useSelector((state) => state.kelurahan_state);
    const dispatch = useDispatch();

    const [state, setState] = React.useState({
        difabel: false,
    });


    React.useEffect(() => {

        setTimeout(() => {
            setLoadingMaried(true);
            setLoading(true);
            setStatus(dataStatus);
            setGenders(dataGender);
            dispatch(payloadProvicePublic());
            dispatch(payloadKabupatenPublic('11'));
            dispatch(payloadKecamatanPublic(''));
            dispatch(payloadKelurahanPublic(''));
            setLoadingMaried(false);
            setLoading(false);
        }, 3000);



    }, [dispatch]);

    const handleChangeDifabel = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
        setDifabel(event.target.checked)
    };

    function handleChange(event, prop) {
        const value = event.target.value;

        if (prop === 'kode_province') {
            setKodeProvince(value);
            dispatch(payloadKabupatenPublic(value));
        }

        if (prop === 'kode_kabupaten') {
            setKodeKabupaten(value);
            dispatch(payloadKecamatanPublic(value));
        }

        if (prop === 'kode_kecamatan') {
            setKodeKecamatan(value);
            dispatch(payloadKelurahanPublic(value));
        }
    };

    const handleSubmit = async () => {

        if (!validator.allValid()) {

            setErrors(validator.getErrorMessages());

            showMessage(false, 'Invalid format data.');

            return false;

        }

        setLoading(true);

        let formData = new FormData();
     
        formData.append('nkk', nkk);
        formData.append('nik', nik);
        formData.append('name', name);
        formData.append('jenis_kelamin', gender);
        formData.append('tempat_lahir', place);
        formData.append('tanggal_lahir', moment(date).format('YYYY-MM-DD'));
        formData.append('alamat', address);
        formData.append('rt', rt);
        formData.append('rw', rw);
        formData.append('kode_provinsi', kodeProvince);
        formData.append('kode_kabupaten', kodeKabupaten);
        formData.append('kode_kecamatan', kodeKecamatan);
        formData.append('kode_desa', kodeDesa);
        formData.append('kawin', maried);
        formData.append('difabel', difabel);
        formData.append('tps', tps);

        Api.putFile('/voters', {
            method: 'POST',
            body: formData
        }).then(resp => {

            setLoading(false);

            showMessage(true, 'Data Pendukung berhasil dibuat.');

            handleClear();

            setErrors([]);

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                setLoading(false);
                setErrors(err.errors);

                showMessage(false, 'Invalid format data');
            }
        });

    };

    const handleClear = () => {
        setNKK('');
        setNik('');
        setName('');
        setGender('L');
        setPlace('');
        setDate(null);
        setAddress('');
        setRt('');
        setRw('');
        setKodeProvince('');
        setKodeKabupaten('');
        setKodeKecamatan('');
        setKodeDesa('');
        setMaried('B');
        setDifabel(0);
        setTps('');
    };

    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    return (
        <div>
            <AppBar position="static" color="default">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        Tambah Data
                    </Typography>
                </Toolbar>
            </AppBar>
            <div className="col-12 mt-3 px-lg-5">
                <div className="form-group">
                    <label>Nomer Kartu Keluarga <span className="required">*</span></label>
                    <TextField variant="outlined"
                        type='text'
                        id='nkk'
                        name="nkk"
                        label="Nomer Kartu Keluarga"
                        onChange={e => setNKK(e.target.value)}
                        value={nkk}
                        fullWidth
                    />
                    {validator.message('nkk', nkk, 'required|numeric|min:16')}
                    <div className='text-danger'>{errors.nkk}</div>
                </div>

                <div className="form-group">
                    <label>Nomer Induk Keluarga <span className="required">*</span></label>
                    <TextField variant="outlined"
                        type='text'
                        id='nik'
                        name="nik"
                        label="Nomer Induk Keluarga"
                        onChange={e => setNik(e.target.value)}
                        value={nik}
                        fullWidth
                    />
                    {validator.message('nik', nik, 'required|numeric|min:16')}
                    <div className='text-danger'>{errors.nik}</div>
                </div>

                <div className="form-group">
                    <label>Nama <span className="required">*</span></label>
                    <TextField variant="outlined"
                        type='text'
                        id='name'
                        name="name"
                        label="Nama"
                        onChange={e => setName(e.target.value)}
                        value={name}
                        fullWidth
                    />
                    {validator.message('name', name, 'required')}
                    <div className='text-danger'>{errors.name}</div>
                </div>
                <div className="row">
                    <div className="col-md-6 col-sm-6 col-6">
                        <div className="form-group">
                            <label>Tempat Lahir <span className="required">*</span></label>
                            <TextField variant="outlined"
                                type='text'
                                id='place'
                                name="place"
                                label="Tempat Lahir"
                                onChange={e => setPlace(e.target.value)}
                                value={place}
                                fullWidth
                            />
                            {validator.message('place', place, 'required')}
                            <div className='text-danger'>{errors.place}</div>
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-6 col-6">
                        <div className='form-group'>
                            <label>Tanggal Lahir</label>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker
                                    label='Tanggal Lahir'
                                    value={date}
                                    onChange={(event) => setDate(event)}
                                    format={'dd MMM yyyy'}
                                    cancelLabel='BATAL'
                                    inputVariant='outlined'
                                />
                            </MuiPickersUtilsProvider>
                            {date &&
                                <button
                                    type='button'
                                    onClick={(event) => setDate(event)}
                                    className='btn btn-delete-select'><i className='fas fa-times-circle'> </i>
                                </button>
                            }
                            <div className='text-danger'>{errors.date}</div>
                        </div>
                    </div>
                </div>

                <div className="form-group ml-3">
                    <label>Status Kawin<span className="required">*</span></label>
                    <RadioGroup
                        row={!loadingMaried}
                        aria-labelledby="demo-radio-buttons-group-label"
                        defaultValue={maried}
                        name="radio-buttons-group"
                    >
                        {loadingMaried ? (
                            <Skeleton variant="text" />
                        ) : (
                            status.length == 0 ? (
                                <div className='selected-empty'>Belum Ada Data</div>
                            ) : (

                                status.map(row => (
                                    <FormControlLabel
                                        name="pernikahan"
                                        onChange={e => setMaried(e.target.value)}
                                        value={row.id.toString()}
                                        control={<Radio />}
                                        label={row.name}
                                        key={row.id} />
                                ))
                            )
                        )}

                    </RadioGroup>
                </div>

                <div className="form-group ml-3">
                    <label>Jenis Kelamin<span className="required">*</span></label>
                    <RadioGroup
                        row={!loading}
                        aria-labelledby="demo-radio-buttons-group-label"
                        defaultValue={gender}
                        name="radio-buttons-group"
                    >
                        {loading ? (
                            <Skeleton variant="text" />
                        ) : (
                            genders.length == 0 ? (
                                <div className='selected-empty'>Belum Ada Data</div>
                            ) : (

                                genders.map(row => (

                                    <FormControlLabel
                                        name="gender"
                                        onChange={e => setGender(e.target.value)}
                                        value={row.id.toString()}
                                        control={<Radio />}
                                        label={row.name}
                                        key={row.id} />


                                ))
                            )
                        )}
                    </RadioGroup>
                    {validator.message('gender', gender, 'required')}
                    <div className='text-danger'>{errors.gender}</div>
                </div>

                <div className="form-group">
                    <label>Difabel <span className="required">*</span></label>
                    <Switch
                        checked={state.difabel}
                        onChange={handleChangeDifabel}
                        name="difabel"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                    />
                    {validator.message('difabel', difabel, 'required')}
                    <div className='text-danger'>{errors.difabel}</div>
                </div>

                <div className="form-group">
                    <label>Provinsi <span className="required">*</span></label>
                    <TextField variant='outlined'
                        select
                        id='kode_province'
                        name='kode_province'
                        label='Provinsi'
                        onChange={e => handleChange(e, 'kode_province')}
                        value={kodeProvince}
                        defaultValue={''}
                        fullWidth
                    >
                        {loadingProvince ?
                            (<CircularProgress />) : dataProvince ? (dataProvince.map(option => (
                                <MenuItem key={parseInt(option.id)} value={option.kode_provinsi}>
                                    {option.province}
                                </MenuItem>
                            ))) : errorProvince ? (<p>{errorProvince}</p>) : (<p>Data Not Found!</p>)}
                    </TextField>
                    {validator.message('kodeProvince', kodeProvince, 'required')}
                    <div className='text-danger'>{errors.kodeProvince}</div>
                </div>

                <div className="form-group">
                    <label>Kabupaten <span className="required">*</span></label>
                    <TextField variant='outlined'
                        select
                        id='kode_kabupaten'
                        name='kode_kabupaten'
                        label='Kabupaten'
                        onChange={e => handleChange(e, 'kode_kabupaten')}
                        value={kodeKabupaten}
                        fullWidth
                    >
                        {loadingKabupaten ?
                            (<CircularProgress />) : dataKabupaten ? (dataKabupaten.map(option => (
                                <MenuItem key={parseInt(option.id)} value={option.kode_kabupaten}>
                                    {option.kabupaten}
                                </MenuItem>
                            ))) : errorKabupaten ? (<p>{errorKabupaten}</p>) : (<p>Data Not Found!</p>)}
                    </TextField>
                    {validator.message('kodeKabupaten', kodeKabupaten, 'required')}
                    <div className='text-danger'>{errors.kodeKabupaten}</div>
                </div>

                <div className="form-group">
                    <label>Kecamatan <span className="required">*</span></label>
                    <TextField variant='outlined'
                        select
                        id='kode_kecamatan'
                        name='kode_kecamatan'
                        label='Kecamatan'
                        onChange={e => handleChange(e, 'kode_kecamatan')}
                        value={kodeKecamatan}
                        fullWidth
                    >
                        {loadingKecamatan ?
                            (<CircularProgress />) : dataKecamatan ? (dataKecamatan.map(option => (
                                <MenuItem key={parseInt(option.id)} value={option.kode_kecamatan}>
                                    {option.kecamatan}
                                </MenuItem>
                            ))) : errorKecamatan ? (<p>{errorKecamatan}</p>) : (<p>Data Not Found!</p>)}
                    </TextField>
                    {validator.message('kodeKecamatan', kodeKecamatan, 'required')}
                    <div className='text-danger'>{errors.kodeKecamatan}</div>
                </div>

                <div className="form-group">
                    <label>Desa <span className="required">*</span></label>
                    <TextField variant='outlined'
                        select
                        id='kode_desa'
                        name='kode_desa'
                        label='Desa'
                        onChange={e => setKodeDesa(e.target.value)}
                        value={kodeDesa}
                        fullWidth
                    >
                        {loadingKelurahan ?
                            (<CircularProgress />) : dataKelurahan ? (dataKelurahan.map(option => (
                                <MenuItem key={parseInt(option.id)} value={option.kode_kelurahan}>
                                    {option.kelurahan}
                                </MenuItem>
                            ))) : errorKelurahan ? (<p>{errorKelurahan}</p>) : (<p>Data Not Found!</p>)}
                    </TextField>
                    {validator.message('kodeDesa', kodeDesa, 'required')}
                    <div className='text-danger'>{errors.kodeDesa}</div>
                </div>

                <div className="form-group">
                    <label>Alamat <span className="required">*</span></label>
                    <TextField variant="outlined"
                        type='text'
                        id='address'
                        name="address"
                        label="Alamat"
                        onChange={e => setAddress(e.target.value)}
                        value={address}
                        fullWidth
                    />
                    {validator.message('address', address, 'required')}
                    <div className='text-danger'>{errors.address}</div>
                </div>

                <div className="row">
                    <div className="col-md-6 col-sm-6 col-6">
                        <div className="form-group">
                            <label>RT <span className="required">*</span></label>
                            <TextField variant="outlined"
                                type='text'
                                id='rt'
                                name="rt"
                                label="RT"
                                onChange={e => setRt(e.target.value)}
                                value={rt}
                                fullWidth
                            />
                            {validator.message('rt', rt, 'required|numeric')}
                            <div className='text-danger'>{errors.rt}</div>
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-6 col-6">
                        <div className="form-group">
                            <label>RW <span className="required">*</span></label>
                            <TextField variant="outlined"
                                type='text'
                                id='rw'
                                name="rw"
                                label="RW"
                                onChange={e => setRw(e.target.value)}
                                value={rw}
                                fullWidth
                            />
                            {validator.message('rw', rw, 'required|numeric')}
                            <div className='text-danger'>{errors.rw}</div>
                        </div>
                    </div>
                </div>

                <div className="form-group">
                    <label>TPS <span className="required">*</span></label>
                    <TextField variant="outlined"
                        type='text'
                        id='tps'
                        name="tps"
                        label="TPS"
                        onChange={e => setTps(e.target.value)}
                        value={tps}
                        fullWidth
                    />
                    {validator.message('tps', tps, 'required|numeric')}
                    <div className='text-danger'>{errors.tps}</div>
                </div>
            </div>
            <div className="col-12 text-left mb-4 mt-5">
                <Button
                    variant="contained"
                    color="primary"
                    className=""
                    onClick={handleSubmit}
                    disabled={loading}
                    fullWidth
                >
                    Simpan{loading && <i className="fa fa-spinner fa-spin"> </i>}
                </Button>
            </div>
            <ToastContainer />
        </div>
    );

}