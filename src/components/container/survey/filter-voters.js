import React from "react";
import { Button } from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from "@material-ui/core/TextField";
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useDispatch, useSelector } from "react-redux";
import { payloadKecamatan } from "../../../redux/actions/kecamatan";
import { payloadKelurahan } from "../../../redux/actions/kelurahan";

const FilterVoters = (props) => {

    const [kodeProvince, setKodeProvince] = React.useState('11');
    const [kodeKabupaten, setKodeKabupaten] = React.useState(props.kabupaten);
    const [kodeKecamatan, setKodeKecamatan] = React.useState('');
    const [kodeDesa, setKodeDesa] = React.useState('');

    const { loadingProvince, errorProvince, dataProvince } = useSelector((state) => state.province_state);
    const { loadingKabupaten, errorKabupaten, dataKabupaten } = useSelector((state) => state.kabupaten_state);
    const { loadingKecamatan, errorKecamatan, dataKecamatan } = useSelector((state) => state.kecamatan_state);
    const { loadingKelurahan, errorKelurahan, dataKelurahan } = useSelector((state) => state.kelurahan_state);
    const dispatch = useDispatch();

    const handleClose = () => {
        props.handleClose();
    };

    const handleSubmitFilter = () => {
        props.handleSubmitFilter(kodeProvince, kodeKabupaten, kodeKecamatan, kodeDesa);
    };

    function handleChange(event, prop){
        const value = event.target.value;

        if (prop === 'kode_province') {
            setKodeProvince(value);
        }

        if (prop === 'kode_kabupaten') {
            setKodeKabupaten(value);
            dispatch(payloadKecamatan(value));
        }

        if (prop === 'kode_kecamatan') {
            setKodeKecamatan(value);
            dispatch(payloadKelurahan(value));
        }
    };
    
    return (
        <Dialog
            maxWidth='sm'
            fullWidth={true}
            open={props.filterOpen}
            onClose={handleClose}
            aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Filter Area</DialogTitle>
            <DialogContent>
                <div className="form-group">
                    <label>Provinsi <span className="required">*</span></label>
                    <TextField variant='outlined'
                        select
                        id='kode_province'
                        name='kode_province'
                        label='Provinsi'
                        onChange={e => handleChange(e, 'kode_province')}
                        value={kodeProvince}
                        defaultValue={''}
                        fullWidth
                    >
                        {loadingProvince ?
                                (<CircularProgress />) : dataProvince ? (dataProvince.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_provinsi}>
                                        {option.province}
                                    </MenuItem>
                                ))) : errorProvince ? (<p>{errorProvince}</p>) : (<p>Data Not Found!</p>)}
                    </TextField>
        
                </div>

                <div className="form-group">
                    <label>Kabupaten </label>
                    <TextField variant='outlined'
                        select
                        id='kode_kabupaten'
                        name='kode_kabupaten'
                        label='Kabupaten'
                        onChange={e => handleChange(e, 'kode_kabupaten')}
                        value={props.kabupaten}
                        defaultValue={''}
                        fullWidth
                    >
                              {loadingKabupaten ?
                                (<CircularProgress />) : dataKabupaten ? (dataKabupaten.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_kabupaten}>
                                        {option.kabupaten}
                                    </MenuItem>
                                ))) : errorKabupaten ? (<p>{errorKabupaten}</p>) : (<p>Data Not Found!</p>)}
                    </TextField>
                </div>

                <div className="form-group">
                    <label>Kecamatan</label>
                    <TextField variant='outlined'
                        select
                        id='kode_kecamatan'
                        name='kode_kecamatan'
                        label='Kecamatan'
                        onChange={e => handleChange(e, 'kode_kecamatan')}
                        value={kodeKecamatan}
                        fullWidth
                    >
                       {loadingKecamatan ?
                                (<CircularProgress />) : dataKecamatan ? (dataKecamatan.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_kecamatan}>
                                        {option.kecamatan}
                                    </MenuItem>
                                ))) : errorKecamatan ? (<p>{errorKecamatan}</p>) : (<p>Data Not Found!</p>)}
                    </TextField>
                </div>

                <div className="form-group">
                    <label>Desa</label>
                    <TextField variant='outlined'
                        select
                        id='kode_desa'
                        name='kode_desa'
                        label='Desa'
                        onChange={e => setKodeDesa(e.target.value)}
                        value={kodeDesa}
                        fullWidth
                    >
                        {loadingKelurahan ?
                                (<CircularProgress />) : dataKelurahan ? (dataKelurahan.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_kelurahan}>
                                        {option.kelurahan}
                                    </MenuItem>
                                ))) : errorKelurahan ? (<p>{errorKelurahan}</p>) : (<p>Data Not Found!</p>)}
                    </TextField>
                </div>

            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="default" className="btn btn-info" onClick={handleClose}>
                    Keluar
                </Button>
                <Button
                    variant="contained"
                    color="primary"
                    className="btn btn-info"
                    onClick={handleSubmitFilter}
                    disabled={props.loadingFilter}
                >
                    Terapkan{props.loadingFilter && <i className="fa fa-spinner fa-spin"> </i>}
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default FilterVoters;