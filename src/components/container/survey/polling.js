import React from "react";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import { toast, ToastContainer } from "react-toastify";
import TextField from "@material-ui/core/TextField";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Skeleton from '@material-ui/lab/Skeleton';
import MenuItem from '@material-ui/core/MenuItem';
import Storage from "../../../libraries/storage";
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CircularProgress from '@material-ui/core/CircularProgress';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import FilterListIcon from '@material-ui/icons/FilterList';
import ClearIcon from '@material-ui/icons/Clear';
import { useDispatch, useSelector } from "react-redux";
import { payloadProvice } from "../../../redux/actions/area";
import { payloadKabupaten } from "../../../redux/actions/kabupaten";
import { payloadKecamatan } from "../../../redux/actions/kecamatan";
import { payloadKelurahan } from "../../../redux/actions/kelurahan";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';


require('dotenv').config();

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '36ch',
        backgroundColor: theme.palette.background.paper,
    },
    container: {
        marginTop: 30,
        width: '100%',
        padding: 26,
    },
    inline: {
        display: 'inline',
    },
    active: {
        backgroundColor: "red"
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightBold,
    },
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography component={'span'}>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};


export default function PollingAdd(props) {

    const classes = useStyles();

    const dispatch = useDispatch();

    const { loadingProvince, errorProvince, dataProvince } = useSelector((state) => state.province_state);
    const { loadingKabupaten, errorKabupaten, dataKabupaten } = useSelector((state) => state.kabupaten_state);
    const { loadingKecamatan, errorKecamatan, dataKecamatan } = useSelector((state) => state.kecamatan_state);
    const { loadingKelurahan, errorKelurahan, dataKelurahan } = useSelector((state) => state.kelurahan_state);

    const [kodeFilter, setKodeFilter] = React.useState(['11']);
    const [loading, setLoading] = React.useState(false);
    const [loadingSimpan, setLoadingSimpan] = React.useState(false);
    const [loadingSearch, setLoadingSearch] = React.useState(false);
    const [search, setSearch] = React.useState('');
    const [voters, setVoters] = React.useState([]);
    const [idVoter, setIdVoter] = React.useState('');
    const [animation, setAnimation] = React.useState('open');
    const [display, setDisplay] = React.useState(false);
    const [isSearch, setIsSearch] = React.useState(false);
    const [dataPresiden, setDataPresiden] = React.useState([]);
    const [dataGubernur, setDataGubernur] = React.useState([]);
    const [dataBupati, setDataBupati] = React.useState([]);
    const [dataDPR, setDataDPR] = React.useState([]);
    const [dataDPD, setDataDPD] = React.useState([]);
    const [dataDPRA, setDataDPRA] = React.useState([]);
    const [dataDPRK, setDataDPRK] = React.useState([]);
    const [status, setStatus] = React.useState([]);
    const [idPresiden, setIdPresiden] = React.useState('');
    const [idBupati, setIdBupati] = React.useState('');
    const [idGubernur, setIdGubernur] = React.useState('');
    const [idDPR, setIdDPR] = React.useState('');
    const [idDPD, setIdDPD] = React.useState('');
    const [idDPRA, setIdDPRA] = React.useState('');
    const [idDPRK, setIdDPRK] = React.useState('');
    const [partaiDPR, setPartaiDPR] = React.useState('');
    const [partaiDPD, setPartaiDPD] = React.useState('');
    const [partaiDPRA, setPartaiDPRA] = React.useState('');
    const [partaiDPRK, setPartaiDPRK] = React.useState('');

    const [statusPresiden, setStatusPresiden] = React.useState('');
    const [statusGubernur, setStatusGubernur] = React.useState('');
    const [statusBupati, setStatusBupati] = React.useState('');
    const [statusDPR, setStatusDPR] = React.useState('');
    const [statusDPD, setStatusDPD] = React.useState('');
    const [statusDPRA, setStatusDPRA] = React.useState('');
    const [statusDPRK, setStatusDPRK] = React.useState('');

    const [infoPresiden, setInfoPresiden] = React.useState('');
    const [infoGubernur, setInfoGubernur] = React.useState('');
    const [infoBupati, setInfoBupati] = React.useState('');
    const [infoDPR, setInfoDPR] = React.useState('');
    const [infoDPD, setInfoDPD] = React.useState('');
    const [infoDPRA, setInfoDPRA] = React.useState('');
    const [infoDPRK, setInfoDPRK] = React.useState('');

    const [partai, setPartai] = React.useState([]);
    const [filterOpen, setFilterOpen] = React.useState(false);
    const [areaFilter, setAreaFilter] = React.useState([]);

    const [kodeProvince, setKodeProvince] = React.useState('11');
    const [kodeKabupaten, setKodeKabupaten] = React.useState('');
    const [kodeKecamatan, setKodeKecamatan] = React.useState('');
    const [kodeDesa, setKodeDesa] = React.useState('');
    const [subCampaign, setSubCampaign] = React.useState('');
    const [pemilihan, setPemilihan] = React.useState('');


    const dataStatus = [
        {
            id: 'Pasti dipilih',
            name: "Pasti dipilih"
        },
        {
            id: 'Mungkin dipilih',
            name: "Mungkin dipilih"
        },
        {
            id: 'Tidak yakin',
            name: "Tidak yakin"
        }
    ];


    React.useEffect(() => {

        const token = props.match.params.id;

        setLoading(true);

        setTimeout(() => {

            dispatch(payloadProvice());
            dispatch(payloadKabupaten());
            dispatch(payloadKecamatan(''));
            dispatch(payloadKelurahan(''));

        }, 3000);

        Storage.set('access_token', token);

        Api.get('/profile').then(resp => {

            if (resp.campaign) {
                const campaign = resp.campaign;
                const subCampaign = campaign.subgroup_campaign_id;
                setSubCampaign(parseInt(subCampaign));

                if (campaign.kode_kabupaten != '') {
                    setKodeKabupaten(campaign.kode_kabupaten);
                    var kode = [...kodeFilter];
                    kode = [...kodeFilter, campaign.kode_kabupaten];
                    setKodeFilter(kode);
                }


                switch (parseInt(subCampaign)) {
                    case 1:
                        setStatusGubernur('Pasti dipilih');
                        break;
                    case 2:
                        setStatusBupati('Pasti dipilih');
                        break;
                    case 3:
                        setStatusDPR('Pasti dipilih');
                        break;
                    case 4:
                        setStatusDPD('Pasti dipilih');
                        break;
                    case 5:
                        setStatusDPRA('Pasti dipilih');
                        break;
                    case 6:
                        setStatusDPRK('Pasti dipilih');
                        break;
                    case 7:
                        setStatusPresiden('Pasti dipilih');
                        break;
                    default:

                }

                setPemilihan(campaign.pemilihan);

            }

        }).catch(err => {
            console.log(err);
        });


        Api.get('/select/campaigns?subgroup_campaign_id=1').then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataGubernur(data);

                setLoading(false);

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/select/campaigns?subgroup_campaign_id=2').then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataBupati(data);

                setLoading(false);

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/select/campaigns?subgroup_campaign_id=3').then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPR(data);

                setLoading(false);

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/select/campaigns?subgroup_campaign_id=4').then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPD(data);

                setLoading(false);

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/select/campaigns?subgroup_campaign_id=5').then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPRA(data);

                setLoading(false);

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/select/campaigns?subgroup_campaign_id=6').then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPRK(data);

                setLoading(false);

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/select/campaigns?subgroup_campaign_id=7').then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataPresiden(data);

                setLoading(false);

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/select/partai').then(resp => {

            if (resp.data) {
                const data = resp.data;

                setPartai(data);
            }

        }).catch(err => {
            console.log(err);
        });


        setStatus(dataStatus);

    }, [dispatch]);


    const fetchCampaignDPR = async (value) => {

        Api.get('/select/campaigns?subgroup_campaign_id=3&partai=' + value).then(resp => {

            if (resp.data) {

                const data = resp.data;

                setDataDPR(data);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchCampaignDPD = async (value) => {

        Api.get('/select/campaigns?subgroup_campaign_id=4&partai=' + value).then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPD(data);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchCampaignDPRA = async (value) => {

        Api.get('/select/campaigns?subgroup_campaign_id=5&partai=' + value).then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPRA(data);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchCampaignDPRK = async (value) => {

        Api.get('/select/campaigns?subgroup_campaign_id=6&partai=' + value).then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPRK(data);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchVoters = async () => {

        let formData = new FormData();
        formData.append('cari', search);
        formData.append('kodeKabupaten', kodeKabupaten);
        formData.append('kodeKecamatan', kodeKecamatan);
        formData.append('kodeDesa', kodeDesa);

        Api.putFile('/search/voters', {
            method: 'POST',
            body: formData
        }).then(resp => {

            const data = resp.data;

            setVoters(data);

            setLoading(false);

            setLoadingSearch(false);

            if (data.length === 0) {
                setIsSearch(true);
            }

        }).catch(err => {

            setLoadingSearch(false);

            setLoading(false);

            showMessage(false, err.message);

        });

    };

    const clearVoters = async () => {

        let formData = new FormData();

        Api.putFile('/search/voters', {
            method: 'POST',
            body: formData
        }).then(resp => {

            const data = resp.data;

            setVoters(data);

            setLoading(false);

            setLoadingSearch(false);

            if (data.length === 0) {
                setIsSearch(true);
            }

        }).catch(err => {

            setLoadingSearch(false);

            setLoading(false);

            showMessage(false, err.message);

        });

    };

    const handleSubmit = async () => {

        const params = [
            {
                'campaign_id': idPresiden,
                'partai_id': null,
                'status': statusPresiden,
                'info': infoPresiden,
            },
            {
                'campaign_id': idGubernur,
                'partai_id': null,
                'status': statusGubernur,
                'info': infoGubernur,
            },
            {
                'campaign_id': idBupati,
                'partai_id': null,
                'status': statusBupati,
                'info': infoBupati,
            },
            {
                'campaign_id': idDPR,
                'partai_id': partaiDPR,
                'status': statusDPR,
                'info': infoDPR,
            },
            {
                'campaign_id': idDPD,
                'partai_id': partaiDPD,
                'status': statusDPD,
                'info': infoDPD,
            },
            {
                'campaign_id': idDPRA,
                'partai_id': partaiDPRA,
                'status': statusDPRA,
                'info': infoDPRA,
            },
            {
                'campaign_id': idDPRK,
                'partai_id': partaiDPRK,
                'status': statusDPRK,
                'info': infoDPRK,
            }
        ];

        setLoadingSimpan(true);

        let formData = new FormData();
        formData.append('survey', JSON.stringify(params));
        formData.append('voter_id', idVoter);

        Api.putFile('/campaign-voters/add', {
            method: 'POST',
            body: formData
        }).then(resp => {

            setLoadingSimpan(false);

            setAnimation('close');

            setDisplay(false)

            showMessage(true, 'Pendukung berhasil ditambahkan');

            fetchVoters();

        }).catch(err => {

            setLoadingSimpan(false);

            setAnimation('close');

            setDisplay(false);

            setLoading(false);

            showMessage(false, err.message);

            fetchVoters();

        });


    };

    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };


    const handleChangeList = (event, newValue) => {
        setIdVoter(newValue);
        const tempVoters = voters.filter(data => data.id === newValue);

        setVoters(tempVoters);
        setAnimation('open')
        setDisplay(true)
    };


    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            setAnimation('close');
            setDisplay(false);
            fetchVoters();
        }
    };

    const handleSearch = () => {
        setAnimation('close');
        setDisplay(false);
        fetchVoters();
    };

    function handleChangePartai(e, prop) {

        const value = e.target.value;

        if (prop === 'dpr') {
            setPartaiDPR(value);
            fetchCampaignDPR(value);
        }

        if (prop === 'dpd') {
            setPartaiDPD(value);
            fetchCampaignDPD(value);
        }

        if (prop === 'dpra') {
            setPartaiDPRA(value);
            fetchCampaignDPRA(value);
        }

        if (prop === 'dprk') {
            setPartaiDPRK(value);
            fetchCampaignDPRK(value);
        }

    };

    const handleOpen = () => {
        setFilterOpen(true);
    };

    const handleClose = () => {
        setFilterOpen(false);
    };

    const handleClearFilter = () => {
        setAreaFilter([]);
        setSearch('');
        setKodeKabupaten('');
        setKodeKecamatan('');
        setKodeDesa('');
        clearVoters();
    };



    function handleChange(event, prop) {
        const value = event.target.value;

        var kode = [...kodeFilter];
        kode = [...kodeFilter, value];

        setKodeFilter(kode);

        if (prop === 'kode_province') {
            setKodeProvince(value);
        }

        if (prop === 'kode_kabupaten') {
            setKodeKabupaten(value);
            dispatch(payloadKecamatan(value));
        }

        if (prop === 'kode_kecamatan') {
            setKodeKecamatan(value);
            dispatch(payloadKelurahan(value));
        }

        if (prop === 'kode_desa') {
            setKodeDesa(value);
        }

    };

    const handleSubmitFilter = () => {

        setTimeout(() => {

            let formData = new FormData();
            kodeFilter.forEach(kd => formData.append('kode[]', kd))

            Api.putFile('/find/areas', {
                method: 'POST',
                body: formData
            }).then(resp => {

                setAreaFilter(Object.keys(resp).map(key => resp[key]));

            }).catch(err => {
                console.log(err);
            });

            setFilterOpen(false);

            fetchVoters();

        }, 500);


    };


    return (
        <div className="row main-content">

            <div className={classes.container}>
                <TextField
                    id="input-with-icon-textfield"
                    variant="outlined"
                    className="search-field"
                    onChange={e => setSearch(e.target.value)}
                    onKeyDown={handleKeyDown}
                    onBlur={handleSearch}
                    placeholder="Cari disini"
                    value={search}
                    fullWidth={true}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="start">
                                <IconButton
                                    aria-label="Search click"
                                    onClick={handleSearch}
                                >
                                    <i className="fas fa-search"> </i>
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />

                <div className="inner-filter">
                    <Button className="filter-btn" onClick={handleOpen}>
                        <FilterListIcon />
                        Filter
                    </Button>
                    {areaFilter.map(option => (
                        <Button className="filter-title" key={option.id}>
                            {option.nama}
                        </Button>
                    ))}
                    {areaFilter.length > 1 && <Button className="clear-btn" onClick={handleClearFilter}>
                        <ClearIcon />
                        Clear
                    </Button>}
                </div>

                <List className={classes.root}>
                    {loadingSearch ? (
                        <div><CircularProgress color="primary" /></div>
                    ) : (
                        (voters.length === 0 && isSearch) ? (
                            <ListItem>
                                <ListItemIcon>
                                    <NotInterestedIcon />
                                </ListItemIcon>
                                <ListItemText
                                    primary="Tidak Ada Data"
                                    secondary=""
                                />
                            </ListItem>
                        ) : (
                            voters.map(option => (
                                <>
                                    <ListItem
                                        key={option.id}
                                        alignItems="flex-start"
                                        selected={option.id == idVoter ? true : false}
                                        onClick={e => handleChangeList(e, option.id)}
                                        classes={{ selected: classes.active }}>

                                        <ListItemText
                                            primary={
                                                <span className="block-area">
                                                    <span className="area">{option.nama_provinsi}</span>
                                                    <span className="area">{option.nama_kabupaten}</span>
                                                    <span className="area">{option.nama_kecamatan}</span>
                                                    <span className="area">{option.nama_desa}</span>
                                                </span>
                                            }
                                            secondary={
                                                <>
                                                    <span className="name">{option.name}</span><br />
                                                    <span className="case">{'Nik: ' + option.nik}</span><br />
                                                    <span className="case">{'Tempat: ' + option.tempat_lahir}</span><br />
                                                    <span className="case">{'Tgllahir: ' + option.tanggal_lahir}</span><br />
                                                    <span className="case">{'Alamat: ' + option.alamat}</span><br />
                                                    <span className="case">{'TPS: ' + option.kode_tps}</span><br />
                                                </>
                                            }
                                        />
                                    </ListItem>
                                    <Divider variant="inset" component="li" />
                                </>
                            )))
                    )}
                </List>

                {display
                    ? <div className={`Modal ${animation}`}>
                        {pemilihan === 'Presiden' &&
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>Presiden</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='presiden'
                                                    name='presiden'
                                                    label='Calon Presiden'
                                                    onChange={e => setIdPresiden(e.target.value)}
                                                    value={idPresiden}
                                                    fullWidth
                                                >
                                                    {dataPresiden.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusPresiden}
                                                value={statusPresiden}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusPresiden(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                        <div className="col-md-12">
                                            <label>Catatan <span className="required">*</span></label>
                                            <TextField variant="outlined"
                                                type='text'
                                                id='outlined-multiline-static-alamat'
                                                name="info"
                                                label="Catatan"
                                                multiline
                                                rows={4}
                                                onChange={e => setInfoPresiden(e.target.value)}
                                                value={infoPresiden}
                                                fullWidth
                                            />
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                        {subCampaign === 1 &&
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel2a-content"
                                    id="panel2a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>Gubernur</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='gubernur'
                                                    name='gubernur'
                                                    label='Calon Gubernur'
                                                    onChange={e => setIdGubernur(e.target.value)}
                                                    value={idGubernur}
                                                    fullWidth
                                                >
                                                    {dataGubernur.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusGubernur}
                                                value={statusGubernur}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusGubernur(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                        <div className="col-md-12">
                                            <label>Catatan <span className="required">*</span></label>
                                            <TextField variant="outlined"
                                                type='text'
                                                id='outlined-multiline-static-alamat'
                                                name="info"
                                                label="Catatan"
                                                multiline
                                                rows={4}
                                                onChange={e => setInfoGubernur(e.target.value)}
                                                value={infoGubernur}
                                                fullWidth
                                            />
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                        {subCampaign === 2 &&
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel3a-content"
                                    id="panel3a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>Bupati</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='bupati'
                                                    name='bupati'
                                                    label='Calon Bupati'
                                                    onChange={e => setIdBupati(e.target.value)}
                                                    value={idBupati}
                                                    fullWidth
                                                >
                                                    {dataBupati.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusBupati}
                                                value={statusBupati}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusBupati(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                        <div className="col-md-12">
                                            <label>Catatan <span className="required">*</span></label>
                                            <TextField variant="outlined"
                                                type='text'
                                                id='outlined-multiline-static-alamat'
                                                name="info"
                                                label="Catatan"
                                                multiline
                                                rows={4}
                                                onChange={e => setInfoBupati(e.target.value)}
                                                value={infoBupati}
                                                fullWidth
                                            />
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                        {subCampaign === 3 &&
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel4a-content"
                                    id="panel4a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>DPR RI</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Partai</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='partai-dpr'
                                                    name='partai-dpr'
                                                    label='Partai'
                                                    onChange={e => handleChangePartai(e, 'dpr')}
                                                    value={partaiDPR}
                                                    fullWidth
                                                >
                                                    {partai.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='dpr'
                                                    name='dpr'
                                                    label='Calon DPR'
                                                    onChange={e => setIdDPR(e.target.value)}
                                                    value={idDPR}
                                                    fullWidth
                                                >
                                                    {dataDPR.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusDPR}
                                                value={statusDPR}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusDPR(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                        <div className="col-md-12">
                                            <label>Catatan <span className="required">*</span></label>
                                            <TextField variant="outlined"
                                                type='text'
                                                id='outlined-multiline-static-alamat'
                                                name="info"
                                                label="Catatan"
                                                multiline
                                                rows={4}
                                                onChange={e => setInfoDPR(e.target.value)}
                                                value={infoDPR}
                                                fullWidth
                                            />
                                        </div>
                                    </div>

                                </AccordionDetails>
                            </Accordion>}
                        {subCampaign === 4 &&
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel5a-content"
                                    id="panel5a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>DPD</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Partai</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='partai-dpd'
                                                    name='partai-dpd'
                                                    label='Partai'
                                                    onChange={e => handleChangePartai(e, 'dpd')}
                                                    value={partaiDPD}
                                                    fullWidth
                                                >
                                                    {partai.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='dpd'
                                                    name='dpd'
                                                    label='Calon DPD'
                                                    onChange={e => setIdDPD(e.target.value)}
                                                    value={idDPD}
                                                    fullWidth
                                                >
                                                    {dataDPD.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusDPD}
                                                value={statusDPD}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusDPD(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                        <div className="col-md-12">
                                            <label>Catatan <span className="required">*</span></label>
                                            <TextField variant="outlined"
                                                type='text'
                                                id='outlined-multiline-static-alamat'
                                                name="info"
                                                label="Catatan"
                                                multiline
                                                rows={4}
                                                onChange={e => setInfoDPD(e.target.value)}
                                                value={infoDPD}
                                                fullWidth
                                            />
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                        {subCampaign === 5 &&
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel6a-content"
                                    id="panel6a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>DPR TK I / DPRA</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Partai</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='partai-dpra'
                                                    name='partai-dpra'
                                                    label='Partai'
                                                    onChange={e => handleChangePartai(e, 'dpra')}
                                                    value={partaiDPRA}
                                                    fullWidth
                                                >
                                                    {partai.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='dpra'
                                                    name='dpra'
                                                    label='Calon DPRA'
                                                    onChange={e => setIdDPRA(e.target.value)}
                                                    value={idDPRA}
                                                    fullWidth
                                                >
                                                    {dataDPRA.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusDPRA}
                                                value={statusDPRA}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusDPRA(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                        <div className="col-md-12">
                                            <label>Catatan <span className="required">*</span></label>
                                            <TextField variant="outlined"
                                                type='text'
                                                id='outlined-multiline-static-alamat'
                                                name="info"
                                                label="Catatan"
                                                multiline
                                                rows={4}
                                                onChange={e => setInfoDPRA(e.target.value)}
                                                value={infoDPRA}
                                                fullWidth
                                            />
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                        {subCampaign === 6 &&
                            <Accordion defaultExpanded={true}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel7a-content"
                                    id="panel7a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>DPR TK II / DPRK</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Partai</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='partai-dprk'
                                                    name='partai-dprk'
                                                    label='Partai'
                                                    onChange={e => handleChangePartai(e, 'dprk')}
                                                    value={partaiDPRK}
                                                    fullWidth
                                                >
                                                    {partai.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='dprk'
                                                    name='dprk'
                                                    label='Calon DPRK'
                                                    onChange={e => setIdDPRK(e.target.value)}
                                                    value={idDPRK}
                                                    fullWidth
                                                >
                                                    {dataDPRK.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusDPRK}
                                                value={statusDPRK}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusDPRK(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                        <div className="col-md-12">
                                            <label>Catatan <span className="required">*</span></label>
                                            <TextField variant="outlined"
                                                type='text'
                                                id='outlined-multiline-static-alamat'
                                                name="info"
                                                label="Catatan"
                                                multiline
                                                rows={4}
                                                onChange={e => setInfoDPRK(e.target.value)}
                                                value={infoDPRK}
                                                fullWidth
                                            />
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                        <div className="inner-button text-left">
                            <Button
                                variant="contained"
                                color="primary"
                                className=""
                                onClick={handleSubmit}
                                disabled={loadingSimpan}
                                fullWidth
                            >
                                Simpan{loadingSimpan && <i className="fa fa-spinner fa-spin"> </i>}
                            </Button>
                        </div>
                    </div> : null}
            </div>

            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">

                </div>
            </div>

            <ToastContainer />

            <Dialog
                maxWidth='sm'
                fullWidth={true}
                open={filterOpen}
                onClose={handleClose}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Filter Area</DialogTitle>
                <DialogContent>
                    <div className="form-group">
                        <label>Provinsi <span className="required">*</span></label>
                        <TextField variant='outlined'
                            select
                            id='kode_province'
                            name='kode_province'
                            label='Provinsi'
                            onChange={e => handleChange(e, 'kode_province')}
                            value={kodeProvince}
                            defaultValue={''}
                            fullWidth
                        >
                            {loadingProvince ?
                                (<CircularProgress />) : dataProvince ? (dataProvince.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_provinsi}>
                                        {option.province}
                                    </MenuItem>
                                ))) : errorProvince ? (<p>{errorProvince}</p>) : (<p>Data Not Found!</p>)}
                        </TextField>

                    </div>

                    <div className="form-group">
                        <label>Kabupaten </label>
                        <TextField variant='outlined'
                            select
                            id='kode_kabupaten'
                            name='kode_kabupaten'
                            label='Kabupaten'
                            onChange={e => handleChange(e, 'kode_kabupaten')}
                            value={kodeKabupaten}
                            defaultValue={''}
                            fullWidth
                        >
                            {loadingKabupaten ?
                                (<CircularProgress />) : dataKabupaten ? (dataKabupaten.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_kabupaten}>
                                        {option.kabupaten}
                                    </MenuItem>
                                ))) : errorKabupaten ? (<p>{errorKabupaten}</p>) : (<p>Data Not Found!</p>)}
                        </TextField>
                    </div>

                    <div className="form-group">
                        <label>Kecamatan</label>
                        <TextField variant='outlined'
                            select
                            id='kode_kecamatan'
                            name='kode_kecamatan'
                            label='Kecamatan'
                            onChange={e => handleChange(e, 'kode_kecamatan')}
                            value={kodeKecamatan}
                            fullWidth
                        >
                            {loadingKecamatan ?
                                (<CircularProgress />) : dataKecamatan ? (dataKecamatan.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_kecamatan}>
                                        {option.kecamatan}
                                    </MenuItem>
                                ))) : errorKecamatan ? (<p>{errorKecamatan}</p>) : (<p>Data Not Found!</p>)}
                        </TextField>
                    </div>

                    <div className="form-group">
                        <label>Desa</label>
                        <TextField variant='outlined'
                            select
                            id='kode_desa'
                            name='kode_desa'
                            label='Desa'
                            onChange={e => handleChange(e, 'kode_desa')}
                            value={kodeDesa}
                            fullWidth
                        >
                            {loadingKelurahan ?
                                (<CircularProgress />) : dataKelurahan ? (dataKelurahan.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_kelurahan}>
                                        {option.kelurahan}
                                    </MenuItem>
                                ))) : errorKelurahan ? (<p>{errorKelurahan}</p>) : (<p>Data Not Found!</p>)}
                        </TextField>
                    </div>

                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="default" className="btn btn-info" onClick={handleClose}>
                        Keluar
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        className="btn btn-info"
                        onClick={handleSubmitFilter}
                        disabled={props.loadingFilter}
                    >
                        Terapkan{props.loadingFilter && <i className="fa fa-spinner fa-spin"> </i>}
                    </Button>
                </DialogActions>
            </Dialog>

            {/* <FilterVoters filterOpen={filterOpen} handleClose={handleClose} loadingFilter={loadingFilter} handleSubmitFilter={handleSubmitFilter} kabupaten={kodeKabupaten} /> */}

        </div>
    )

}

