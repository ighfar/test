import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Api from "../../../libraries/api";
import {
  Button,
  CircularProgress,
  IconButton,
  InputAdornment,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
} from "@material-ui/core";
import TablePaginationActions from "../../presentational/table-pagination-actions";
import ModalPreview from "./ModalPreview";

const Blanko = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [isModal, setIsModal] = useState(false);
  const [fileUrl, setFileUrl] = useState("");
  const [total, setTotal] = React.useState(0);
  const [perPage, setPerPage] = React.useState(50);
  const [currentPage, setCurrentPage] = React.useState(1);
  const [searchBy, setSearchBy] = React.useState("");
  const [currentPageTable, setCurrentPageTable] = React.useState(0);

  useEffect(() => {
    getBlanko();
  }, [currentPage, perPage]);

  const getBlanko = async () => {
    setLoading(true);
    try {
      const response = await Api.post("/voters/campaign", {
        page: currentPage,
        limit: perPage,
        search: searchBy,
      });
      setData(response.data);
      setTotal(response.meta.pagination.total);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    } finally {
      setLoading(false);
    }
  };

  const handleChangePage = (event, newPage) => {
    setCurrentPage(newPage + 1);
    setCurrentPageTable(newPage);
    setLoading(true);
  };

  const handleChangeRowsPerPage = (event) => {
    setPerPage(event.target.value);
    setCurrentPage(1);
    setLoading(true);
  };

  const handleSearch = async () => {
    getBlanko();
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      setSearchBy(event.target.value);
      setLoading(true);
    }
  };

  console.log(data);

  return (
    <>
      <ModalPreview
        isModal={isModal}
        closeButton={() => setIsModal(false)}
        fileUrl={fileUrl}
      />
      <div className="row main-content">
        <div className="col-12 px-lg-5">
          <h1 className="page-title">Blanko</h1>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/">Home</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Blanko
              </li>
            </ol>
          </nav>
        </div>
        <div className="col-12 mt-3 px-lg-5">
          <div className="table-wrapper">
            <div className="row align-items-center mb-md-3">
              <div className="col-md-3 mb-3">
                <TextField
                  id="input-with-icon-textfield"
                  variant="outlined"
                  className="search-field"
                  onChange={(e) => setSearchBy(e.target.value)}
                  onBlur={handleSearch}
                  onKeyDown={handleKeyDown}
                  placeholder="Cari disini"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <IconButton
                          aria-label="Search click"
                          onClick={handleSearch}
                        >
                          <i className="fas fa-search"> </i>
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
              <Table className="table-list" size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>Pemilih</TableCell>
                    <TableCell>Foto KTP</TableCell>
                    <TableCell>Tampilkan</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {loading ? (
                    <TableRow>
                      <TableCell colSpan={16} align="center" className="py-5">
                        <CircularProgress color="primary" />
                      </TableCell>
                    </TableRow>
                  ) : (
                    data.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell>{item.voter_name}</TableCell>
                        <TableCell>
                          <img
                            src={`${process.env.REACT_APP_API_STORAGE_PATH}${item.ktp}`}
                            alt="foto ktp"
                            width={75}
                            height={75}
                          />
                        </TableCell>
                        <TableCell>
                          <Button
                            variant="outlined"
                            onClick={() => {
                              setFileUrl(item.ktp);
                              setIsModal(true);
                            }}
                          >
                            Tampilkan
                          </Button>
                        </TableCell>
                      </TableRow>
                    ))
                  )}
                </TableBody>
              </Table>
              <TablePagination
                rowsPerPageOptions={[10, 15, 25, 50]}
                component="div"
                count={total}
                rowsPerPage={perPage}
                page={currentPageTable}
                backIconButtonProps={{
                  "aria-label": "previous page",
                }}
                nextIconButtonProps={{
                  "aria-label": "next page",
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Blanko;
