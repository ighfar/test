import { Box, Modal } from "@material-ui/core";
import React from "react";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "80vw",
  height: "80vh",
  bgcolor: "white",
  border: "2px solid #000",
  display: "flex",
  justifyContent: "center",
  boxShadow: 24,
  p: 4,
};

const ModalPreview = ({ isModal, closeButton, fileUrl }) => {
  return (
    <Modal open={isModal} onClose={closeButton}>
      <Box sx={style}>
        <img
          src={`${process.env.REACT_APP_API_STORAGE_PATH}${fileUrl}`}
          alt="foto ktp"
        />
      </Box>
    </Modal>
  );
};

export default ModalPreview;
