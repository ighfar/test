import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import { toast, ToastContainer } from "react-toastify";
import TablePagination from "@material-ui/core/TablePagination";
import TablePaginationActions from "../../presentational/table-pagination-actions";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import { history } from "../../../shared/configure-store";
import ErrorHandling from "../../../libraries/error-handling";

require('dotenv').config();

class CapelBlack extends Component {
    _isMounted = false;

    constructor(props) {

        super(props);

        this.state = {
            loading: true,
            loadingButton: false,
            errors: {},

            orderBy: '',
            sortedBy: '',
            searchBy: '',

            roles: [],
            showDialog: false,
            idDelete: '',
            currDelete: '',

            rows: [],
            total: 0,
            perPage: 10,
            currentPage: 1,
            currentPageTable: 0,

            isEdit: false,
            isDelete: false,
            isCreate: false,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {

        document.title = 'Halaman Daftar Tim & Saksi Ditolak';

        this.__fetchData(false);

    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    __fetchData = update => {
        this._isMounted = true;

        let page = update ? parseInt(this.state.currentPage + 1) : this.state.currentPage;

        let sort = this.state.orderBy ? '&orderBy=' + this.state.orderBy + '&sortedBy=' + this.state.sortedBy : '';
        let search = this.state.searchBy ? '&search=' + this.state.searchBy : '';

        let route = '';

        if (search) {
            route = '/timses-black?limit=' + this.state.perPage + sort + search;
        } else {
            route = '/timses-black?limit=' + this.state.perPage + '&page=' + page + sort + search;
        }

        Api.get(route).then(resp => {
            if (this._isMounted) {

                if (resp.data) {

                    this.setState({
                        rows: resp.data,
                        perPage: resp.meta.pagination.per_page,
                        currentPage: resp.meta.pagination.current_page,
                        currentPageTable: resp.meta.pagination.current_page - 1,
                        total: resp.meta.pagination.total,
                        loading: false,
                        isEdit: resp.meta.custom.isEdit,
                        isDelete: resp.meta.custom.isDelete,
                        isCreate: resp.meta.custom.isCreate,
                    })

                }
            }
        }).catch(err => {
            console.log(err);
        });
    };

    handleSearch = () => {
        this.__fetchData(false);
    };
    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.__fetchData(false);
        }
    };

    handleSort = (val) => {

        let sortedBy = this.state.sortedBy;

        if (val !== this.state.orderBy) {
            sortedBy = 'asc';
        } else {
            if (sortedBy && sortedBy === 'asc') {
                sortedBy = 'desc';
            } else {
                sortedBy = 'asc';
            }
        }

        this.setState({
            orderBy: val,
            sortedBy: sortedBy,
            loading: true
        }, () => {
            this.__fetchData(false);
        });
    };

    handleChange(e, prop) {

        this.setState({

            [prop]: e.target.value

        })

    };

    handleOpen = (row) => {
        this.setState({
            showDialog: true,
            idDelete: row.id,
            currDelete: row.name,
        })
    };

    handleClose = () => {
        this.setState({
            showDialog: false,
        })
    };

    handleDelete = () => {

        if (this.state.idDelete) {
            Api.delete('/timses-campaign/' + this.state.idDelete, '', false).then(resp => {

                this.setState({
                    showDialog: false,
                }
                );

                this.showMessage(true, 'Timses Kandidat successfully deleted');
                this.__fetchData(false);

            }).catch(err => {

                this.setState({
                    showDialog: false
                }
                );

                this.showMessage(true, 'Timses Kandidat successfully deleted');
                this.__fetchData(false);
            });
        }
    };

    showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    handleChangePage = (event, newPage) => {
        this.setState({
            rows: [],
            currentPage: newPage,
            currentPageTable: newPage,
            loading: true,
        }, () => {
            this.__fetchData(true);
        });
    };

    handleChangeRowsPerPage = event => {
        this.setState({
            perPage: parseInt(event.target.value, 10),
            currentPage: 1,
            loading: true,
        }, () => {
            this.__fetchData(false);
        });
    };

    handleActive = (id) => {
        const params = {
            timses_campaign: id,
        };
        Api.patch('/timses-campaigns/' + id + '/active', params).then(resp => {

            this.setState({
                loadingButton: false,
            });

            window.location.reload();

            this.showMessage(true, 'Timses kandidat berhasil dirubah.');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                this.setState({
                    errors: err.errors,
                    loadingButton: false
                });

                this.showMessage(false, 'Invalid format data');
            }
        });
    };

    handleGo = (link) => {

        history.push(link);
    };

    render() {
        return (
            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h1 className="page-title">Tim & Saksi Di Tolak</h1>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Tim & Saksi Di Tolak</li>
                        </ol>
                    </nav>

                </div>
                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                        <div className="row align-items-center mb-md-3">
                            <div className="col-md-6">
                                <TextField
                                    id="input-with-icon-textfield"
                                    variant="outlined"
                                    className="search-field"
                                    onChange={(e) => this.handleChange(e, 'searchBy')}
                                    onKeyDown={this.handleKeyDown}
                                    onBlur={this.handleSearch}
                                    placeholder="Cari disini"
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="start">
                                                <IconButton
                                                    aria-label="Search click"
                                                    onClick={this.handleSearch}
                                                >
                                                    <i className="fas fa-search"> </i>
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                            <div className="col-md-6 text-md-right">

                                {this.state.isCreate &&
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        className="mb-3 mb-md-0"
                                        onClick={() => this.handleGo('/categories/add')}
                                    >
                                        Buat Kategori
                                    </Button>
                                }
                            </div>
                        </div>

                        <Table className="table-list" size="small" >
                            <TableHead>
                                <TableRow>
                                    <TableCell>ID#</TableCell>
                                    <TableCell className="clickable" onClick={() => this.handleSort('kandidat')} >Nama Kandidat
                                        {this.state.orderBy === 'kandidat' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell className="clickable" onClick={() => this.handleSort('name')} >Nama Lengkap
                                        {this.state.orderBy === 'name' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell className="clickable" onClick={() => this.handleSort('nick_name')} >Nama Panggilan
                                        {this.state.orderBy === 'nick_name' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell>Tanggal Daftar</TableCell>
                                    <TableCell>Nomer NIK</TableCell>
                                    <TableCell>Nomer HP</TableCell>
                                    <TableCell>Saksi</TableCell>
                                    <TableCell>Nomer TPS</TableCell>
                                    <TableCell>Posisi</TableCell>
                                    <TableCell>Approve</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.loading ? (
                                    <TableRow>
                                        <TableCell colSpan={11} align="center" className="py-5">
                                            <CircularProgress color="primary" />
                                        </TableCell>
                                    </TableRow>
                                ) : (
                                    this.state.rows.length === 0 ? (
                                        <TableRow style={{ height: 33 * this.state.perPage }}>
                                            <TableCell colSpan={11} style={{ textAlign: "center" }}>Belum ada data</TableCell>
                                        </TableRow>
                                    ) : (
                                        this.state.rows.map(row => (
                                            <TableRow
                                                key={row.id}
                                                selected={row.selected}>
                                                <TableCell>
                                                    <span>{row.real_id === null ? '' : row.real_id}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.kandidat === null ? '' : row.kandidat}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.name === null ? '' : row.name}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.nick_name === null ? '' : row.nick_name}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.created_at === null ? '' : row.created_at}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.nik === null ? '' : row.nik}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.phone === null ? '' : row.phone}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.saksi === null ? '' : row.saksi}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.nomer_tps === null ? '' : row.nomer_tps}</span>
                                                </TableCell>
                                                <TableCell>

                                                </TableCell>
                                                <TableCell>
                                                    {row.status === null ? '' : <button className="btn btn-secondary" aria-disabled="true">
                                                        {row.status}
                                                    </button>}
                                                </TableCell>

                                            </TableRow>
                                        ))
                                    )
                                )}

                            </TableBody>
                        </Table>
                    </div>

                    <TablePagination
                        rowsPerPageOptions={[10, 25, 50]}
                        component="div"
                        count={this.state.total}
                        rowsPerPage={this.state.perPage}
                        page={this.state.currentPageTable}
                        backIconButtonProps={{
                            'aria-label': 'previous page',
                        }}
                        nextIconButtonProps={{
                            'aria-label': 'next page',
                        }}
                        onChangePage={this.handleChangePage}
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                        ActionsComponent={TablePaginationActions}
                    />

                </div>

                <Dialog
                    maxWidth='sm'
                    fullWidth={true}
                    open={this.state.showDialog}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">DELETE TIMSES DARI KANDIDAT: {this.state.currDelete}</DialogTitle>
                    <DialogContent>
                        <p>Kamu yakin mau menghapus kandidat ini?</p>
                    </DialogContent>
                    <DialogActions>
                        <button className="btn btn-info" onClick={this.handleClose}>
                            Batalkan
                        </button>
                        <button className="btn btn-danger" onClick={this.handleDelete}>
                            Hapus
                        </button>
                    </DialogActions>
                </Dialog>

                <ToastContainer />

            </div>
        )
    }
}

export default CapelBlack;