import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CircularProgress from '@material-ui/core/CircularProgress';
import img from '../../../images/home.png';
import { Link } from 'react-router-dom';
import Api from "../../../libraries/api";


export default function ContentDashboard(props) {


    const [loading, setLoading] = React.useState(true);
    const [data, setData] = React.useState([]);
    const [pemilihan, setPemilihan] = React.useState('');

    React.useEffect(() => {

        document.title = 'Admin suarapemilu - Peta Suara Pendukung';

        if (loading) {
            fetchData();
        }



    }, [loading]);

    const fetchData = async () => {

        let route = '/dashboards/peta-suara';

        let formData = new FormData();
        formData.append('skipCache', true);


        Api.putFile(route, {
            method: 'POST',
            body: formData
        }).then(resp => {

            if (resp) {

                const dataSuara = resp.data;
                setLoading(false);
                setData(dataSuara);
                setPemilihan(dataSuara[0].pemilihan)

            }

        }).catch(err => {
            console.log(err);
        });
    };


    return (

        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h2 className="page-title">Peta Suara</h2>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Peta Suara</li>                        </ol>
                </nav>

                <div className="card-home mb-3">
                    <div className="row justify-content-between align-items-center">
                        <div className="col-md-7 ">
                            <blockquote>{pemilihan}</blockquote>
                            <p></p>
                        </div>
                        <div className="col-md-5 col-lg-4">
                            <img src={img} alt='welcome' className="img-fluid" />
                        </div>
                    </div>
                </div>

                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                    
                        <Table className="table-list" size="small" >
                            <TableHead>
                                <TableRow>
                                    <TableCell>Nomer</TableCell>
                                    <TableCell>Kandidat</TableCell>
                                    <TableCell>Pemilihan</TableCell>
                                    <TableCell>Partai</TableCell>
                                    <TableCell>Nomer Urut</TableCell>
                                    <TableCell>Jumlah Suara</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {loading ? (
                                    <TableRow>
                                        <TableCell colSpan={16} align="center" className="py-5">
                                            <CircularProgress color="primary" />
                                        </TableCell>
                                    </TableRow>
                                ) : (
                                    data.length === 0 ? (
                                        <TableRow>
                                            <TableCell colSpan={6} style={{ textAlign: "center" }}>Belum ada data.</TableCell>
                                        </TableRow>
                                    ) : (
                                        data.map((row, key) => (
                                            <TableRow
                                                key={key}
                                                selected={false}>
                                                <TableCell>
                                                    <span>{key+1}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.name === null ? '' : row.name}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.pemilihan === null ? '' : row.pemilihan}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.nama_partai === null ? '' : row.nama_partai}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.campaign_id === null ? '' : row.campaign_id}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.jumlah_suara === null ? '' : row.jumlah_suara}</span>
                                                </TableCell>
                                            </TableRow>
                                        ))
                                    )
                                )}

                            </TableBody>
                        </Table>
                    </div>


                </div>




            </div>

        </div>

    )


}