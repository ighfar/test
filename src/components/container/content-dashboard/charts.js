import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CircularProgress from '@material-ui/core/CircularProgress';
import img from '../../../images/home.png';
import { Link } from 'react-router-dom';
import Api from "../../../libraries/api";


export default function Charts(props) {

    const [loading, setLoading] = React.useState(true);
    const [data, setData] = React.useState([]);
    const [column, setColumn] = React.useState([]);
    const [pemilihan, setPemilihan] = React.useState('');

    React.useEffect(() => {

        document.title = 'Admin suarapemilu - Perhitungan Suara Pendukung';

        if (loading) {
            fetchData();
        }



    }, [loading]);

    const fetchData = async () => {

        let route = '/dashboards/perhitungan';

        let formData = new FormData();
        formData.append('skipCache', true);


        Api.putFile(route, {
            method: 'POST',
            body: formData
        }).then(resp => {

            if (resp) {

                const dataSuara = resp.data.data;
                const column = resp.data.column;
                setLoading(false);
                setData(dataSuara);
                setColumn(column);
                setPemilihan(dataSuara[0].pemilihan)

            }

        }).catch(err => {
            console.log(err);
        });
    };


    return (

        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h2 className="page-title">Perhitungan Suara Pendukung</h2>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Suara Pendukung</li>                        </ol>
                </nav>

                <div className="card-home mb-3">
                    <div className="row justify-content-between align-items-center">
                        <div className="col-md-7 ">
                            <blockquote>{pemilihan}</blockquote>
                            <p></p>
                        </div>
                        <div className="col-md-5 col-lg-4">
                            <img src={img} alt='welcome' className="img-fluid" />
                        </div>
                    </div>
                </div>

                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">

                        <Table className="table-list pendukung">
                            <TableHead>
                                <TableRow>
                                    <TableCell style={{ width: 100 }}>Kabupaten</TableCell>
                                    <TableCell>Kecamatan</TableCell>
                                    <TableCell>Desa</TableCell>
                                    <TableCell>Total Pendukung</TableCell>
                                    {
                                        column.map((row, key) => (
                                            <TableCell>{row.name}</TableCell>
                                        ))
                                    }
                                    
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {loading ? (
                                    <TableRow>
                                        <TableCell colSpan={16} align="center" className="py-5">
                                            <CircularProgress color="primary" />
                                        </TableCell>
                                    </TableRow>
                                ) : (
                                    data.length === 0 ? (
                                        <TableRow>
                                            <TableCell colSpan={6} style={{ textAlign: "center" }}>Belum ada data.</TableCell>
                                        </TableRow>
                                    ) : (
                                        data.map((row, key) => (
                                            <TableRow
                                                key={key}
                                                selected={false}>
                                                <TableCell>
                                                    <span>{row.kabupaten === null ? '' : row.kabupaten}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.kecamatan === null ? '' : row.kecamatan}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.desa === null ? '' : row.desa}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.total === null ? '' : row.total}</span>
                                                </TableCell>
                                                {
                                                    column.map((col, key) => (
                                                        <TableCell>{row[col.name]}</TableCell>
                                                    ))
                                                }
                                                
                                            </TableRow>
                                        ))
                                    )
                                )}

                            </TableBody>
                        </Table>
                    </div>


                </div>




            </div>

        </div>

    )


}