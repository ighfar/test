import { PAYLOAD_PROVICE, PAYLOAD_KABUPATEN, PAYLOAD_KECAMATAN, PAYLOAD_KELURAHAN } from "../types/area";

const initialState = {
    loadingProvince: true,
    errorProvince: false,
    dataProvince: false,
};

const province = (state = initialState, action) => {
    const {type, payload} = action;
    console.log(action);
    switch(type) {
        case PAYLOAD_PROVICE:
            return {
                ...state,
                loadingProvince: payload.loading,
                errorProvince: payload.errorMessage,
                dataProvince: payload.data,
            }  
        default:
            return state;  
    }
}

export default province;