import { INITIAL_PROVICE, PAYLOAD_PROVICE } from "../types/area";
import Api from "../../libraries/api";

export const payloadProvice = () => {
    return (dispatch) => {
        dispatch({
            type: INITIAL_PROVICE,
            payload: {
                loading: true,
                data: false,
                errorMessage: false
            }
        });

        Api.get('/province/dapil').then(resp => {

            if (resp) {
                dispatch({
                    type: PAYLOAD_PROVICE,
                    payload: {
                        loading: false,
                        data: resp.data,
                        errorMessage: false
                    }
                });
            }

        }).catch(err => {
            dispatch({
                type: PAYLOAD_PROVICE,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: err.message
                }
            });
        });
    }
}