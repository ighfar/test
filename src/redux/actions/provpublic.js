import { INITIAL_PROVICE, PAYLOAD_PROVICE } from "../types/area";
import Api from "../../libraries/api";

export const payloadProvicePublic = () => {
    return (dispatch) => {
        dispatch({
            type: INITIAL_PROVICE,
            payload: {
                loading: true,
                data: false,
                errorMessage: false
            }
        });

        Api.get('/province/areas').then(resp => {

            if (resp) {
                dispatch({
                    type: PAYLOAD_PROVICE,
                    payload: {
                        loading: false,
                        data: resp,
                        errorMessage: false
                    }
                });
            }

        }).catch(err => {
            dispatch({
                type: PAYLOAD_PROVICE,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: err.message
                }
            });
        });
    }
}