import { INITIAL_KABUPATEN, PAYLOAD_KABUPATEN } from "../types/area";
import Api from "../../libraries/api";

export const payloadKabupatenPublic = (provinsi) => {
    return (dispatch) => {
        dispatch({
            type: INITIAL_KABUPATEN,
            payload: {
                loading: true,
                data: false,
                errorMessage: false
            }
        });

        Api.get(`/kabupaten/areas?province=${provinsi}`).then(resp => {

            if (resp) {
                dispatch({
                    type: PAYLOAD_KABUPATEN,
                    payload: {
                        loading: false,
                        data: resp,
                        errorMessage: false
                    }
                });
            }

        }).catch(err => {
            dispatch({
                type: PAYLOAD_KABUPATEN,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: err.message
                }
            });
        });
    }
}