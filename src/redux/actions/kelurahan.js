import { INITIAL_KELURAHAN, PAYLOAD_KELURAHAN } from "../types/area";
import Api from "../../libraries/api";

export const payloadKelurahan = (kecamatan) => {
    return (dispatch) => {
        dispatch({
            type: INITIAL_KELURAHAN,
            payload: {
                loading: true,
                data: false,
                errorMessage: false
            }
        });

        Api.get(`/kelurahan/dapil?kode_kecamatan=${kecamatan}`).then(resp => {

            if (resp) {
                dispatch({
                    type: PAYLOAD_KELURAHAN,
                    payload: {
                        loading: false,
                        data: resp.data,
                        errorMessage: false
                    }
                });
            }

        }).catch(err => {
            dispatch({
                type: PAYLOAD_KELURAHAN,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: err.message
                }
            });
        });
    }
}