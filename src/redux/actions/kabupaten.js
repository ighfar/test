import { INITIAL_KABUPATEN, PAYLOAD_KABUPATEN } from "../types/area";
import Api from "../../libraries/api";

export const payloadKabupaten = () => {
    return (dispatch) => {
        dispatch({
            type: INITIAL_KABUPATEN,
            payload: {
                loading: true,
                data: false,
                errorMessage: false
            }
        });

        Api.get('/kabupaten/dapil').then(resp => {

            if (resp) {
                dispatch({
                    type: PAYLOAD_KABUPATEN,
                    payload: {
                        loading: false,
                        data: resp.data,
                        errorMessage: false
                    }
                });
            }

        }).catch(err => {
            dispatch({
                type: PAYLOAD_KABUPATEN,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: err.message
                }
            });
        });
    }
}