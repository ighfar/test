import { INITIAL_TIMSES, PAYLOAD_TIMSES } from "../types/timses";
import Api from "../../libraries/api";

export const payloadTimses = () => {
    return (dispatch) => {
        dispatch({
            type: INITIAL_TIMSES,
            payload: {
                loading: true,
                data: false,
                errorMessage: false
            }
        });

        Api.get('/campaign/timses').then(resp => {

            if (resp) {
                dispatch({
                    type: PAYLOAD_TIMSES,
                    payload: {
                        loading: false,
                        data: resp.data,
                        errorMessage: false
                    }
                });
            }

        }).catch(err => {
            dispatch({
                type: PAYLOAD_TIMSES,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: err.message
                }
            });
        });
    }
}