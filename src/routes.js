import React from 'react'
import { Provider } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import { history } from './shared/configure-store'
import Dashboard from "./components/container/dashboard"
import Login from "./components/container/login"
import SurveyAdd from "./components/container/survey/add"
import PollingAdd from "./components/container/survey/polling"
import NotFound from "./components/container/not-found"
import { PrivateRoute, RedirectRoute } from './libraries/route-handling'
import TimsesMobile from './components/container/timses/timses'
import AddNewVoters from './components/container/survey/new'

const Routes = ({ store }) => (

    <Provider store={store}>

        <ConnectedRouter history={history}>

            {

                <Switch>

                    <RedirectRoute exact path="/login" component={Login} />

                    <Route
                        exact
                        key="/poling-without-token"
                        path="/polling/:id"
                        component={PollingAdd}
                    />

                    <Route
                        exact
                        key="/public-polling"
                        path="/add-polling/:id"
                        component={PollingAdd}
                    />

                    <Route
                        exact
                        key="/public-survey"
                        path="/survey/:id"
                        component={SurveyAdd}
                    />

                    <Route
                        exact
                        key="/add-pendukung"
                        path="/add-pendukung"
                        component={AddNewVoters}
                    />

                    <Route
                        exact
                        key="/public-timses"
                        path="/timses/:id"
                        component={TimsesMobile}
                    />

                    <PrivateRoute path="/" component={Dashboard} />

                    <Route component={NotFound} />

                </Switch>

            }

        </ConnectedRouter>

    </Provider>

);

export default Routes;