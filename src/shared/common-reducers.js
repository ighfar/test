import createReducer from "../libraries/create-reducer";

import * as types from './common-types';
import { INITIAL_PROVICE, PAYLOAD_PROVICE, INITIAL_KABUPATEN, PAYLOAD_KABUPATEN, INITIAL_KECAMATAN, PAYLOAD_KECAMATAN, INITIAL_KELURAHAN, PAYLOAD_KELURAHAN } from "../redux/types/area";
import { INITIAL_TIMSES, PAYLOAD_TIMSES } from "../redux/types/timses";

let initial_profile_state = {

    id: '',
    email: '',
    fullName: '',
    phone: '',
    firstName: '',
    lastName: '',
    avatar: '',
    lastLogin: '',
    address: '',
    address2: '',
    zipCode: '',
    toggleSidebar: '',

};

export const profile_state = createReducer(initial_profile_state, {

    [types.SET_INITIAL_PROFILE_STATE](state) {

        return {

            id: '',
            email: '',
            fullName: '',
            phone: '',
            firstName: '',
            lastName: '',
            avatar: '',
            lastLogin: '',
            address: '',
            address2: '',
            zipCode: '',

        }

    },

    [types.SET_PROFILE_STATE](state,action) {

        return action.payload

    },

});

let initial_toggle_sidebar_state = false;

export const toggle_sidebar_state = createReducer(initial_toggle_sidebar_state, {

    [types.SET_INITIAL_TOGGLE_SIDEBAR_STATE](state) {

        return false

    },


    [types.SET_SHOW_SIDEBAR](state) {

        return true

    },

    [types.SET_HIDE_SIDEBAR](state) {

        return false

    },

});

let initial_role_state = '';

export const role_state = createReducer(initial_role_state, {

    [types.SET_INITIAL_ROLE_STATE](state) {

        return ''

    },


    [types.SET_ROLE_STATE](state, action) {

        return action.payload

    },

});

let initial_permission_state = '';

export const permission_state = createReducer(initial_permission_state, {

    [types.SET_INITIAL_PERMISSION_STATE](state) {

        return ''

    },


    [types.SET_PERMISSION_STATE](state, action) {

        return action.payload.filter(v =>  v.access).map(v => v.name)

    },

});

let initial_province_state = {
    loadingProvince: true,
    errorProvince: false,
    dataProvince: false,
};

export const province_state = createReducer(initial_province_state, {

    [INITIAL_PROVICE](state) {

        return state;

    },

    [PAYLOAD_PROVICE](state, action) {

        const {payload} = action;

        return {
            ...state,
            loadingProvince: payload.loading,
            errorProvince: payload.errorMessage,
            dataProvince: Object.keys(payload.data).map(key => payload.data[key])
        };

    },

});

let initial_kabupaten_state = {
    loadingKabupaten: true,
    errorKabupaten: false,
    dataKabupaten: false,
};

export const kabupaten_state = createReducer(initial_kabupaten_state, {

    [INITIAL_KABUPATEN](state) {

        return state;

    },

    [PAYLOAD_KABUPATEN](state, action) {

        const {payload} = action;

        return {
            ...state,
            loadingKabupaten: payload.loading,
            errorKabupaten: payload.errorMessage,
            dataKabupaten: Object.keys(payload.data).map(key => payload.data[key])
        };

    },

});

let initial_kecamatan_state = {
    loadingKecamatan: true,
    errorKecamatan: false,
    dataKecamatan: false,
};

export const kecamatan_state = createReducer(initial_kecamatan_state, {

    [INITIAL_KECAMATAN](state) {

        return state;

    },

    [PAYLOAD_KECAMATAN](state, action) {

        const {payload} = action;

        return {
            ...state,
            loadingKecamatan: payload.loading,
            errorKecamatan: payload.errorMessage,
            dataKecamatan: Object.keys(payload.data).map(key => payload.data[key])
        };

    },

});

let initial_kelurahan_state = {
    loadingKelurahan: true,
    errorKelurahan: false,
    dataKelurahan: false,
};

export const kelurahan_state = createReducer(initial_kelurahan_state, {

    [INITIAL_KELURAHAN](state) {

        return state;

    },

    [PAYLOAD_KELURAHAN](state, action) {

        const {payload} = action;

        return {
            ...state,
            loadingKelurahan: payload.loading,
            errorKelurahan: payload.errorMessage,
            dataKelurahan: Object.keys(payload.data).map(key => payload.data[key])
        };

    },

});

let initial_timses_state = {
    loadingTimses: true,
    errorTimses: false,
    dataTimses: false,
};

export const timses_state = createReducer(initial_timses_state, {

    [INITIAL_TIMSES](state) {

        return state;

    },

    [PAYLOAD_TIMSES](state, action) {

        const {payload} = action;

        return {
            ...state,
            loadingTimses: payload.loading,
            errorTimses: payload.errorMessage,
            dataTimses: Object.keys(payload.data).map(key => payload.data[key])
        };

    },

});
